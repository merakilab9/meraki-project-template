package route

import (
	"github.com/caarlos0/env/v6"
	"meraki-project-template/conf"
	_ "meraki-project-template/docs"
	"meraki-project-template/pkg/handlers"
	"meraki-project-template/pkg/model"
	"meraki-project-template/pkg/repo"
	srv "meraki-project-template/pkg/service"

	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
	"gitlab.com/merakilab9/common/ginext"
	"gitlab.com/merakilab9/common/service"
)

type extraSetting struct {
	DbDebugEnable bool `env:"DB_DEBUG_ENABLE" envDefault:"true"`
}

type Service struct {
	*service.BaseApp
	setting *extraSetting
}

// NewService
// @title Swagger Example API
// @version 1.0
// @description This is a sample server celler server.
// @termsOfService http://swagger.io/terms/
// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io
// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html
// @host localhost:8001
// @BasePath /api/v1

func NewService() *Service {
	s := &Service{
		service.NewApp("meraki-project-template", "v1.0"),
		&extraSetting{},
	}
	_ = env.Parse(s.setting)

	db := s.GetDB()
	if s.setting.DbDebugEnable {
		db = db.Debug()
	}

	v1Group := s.Router.Group("/api/v1")

	repoRuleDefault := repo.NewRepository[model.RuleDefault](db, "rule_default")
	repoRatingGroup := repo.NewRepository[model.RatingGroup](db, "rating_group")
	repoRating := repo.NewRepository[model.Rating](db, "rating")
	repoRatingDetail := repo.NewRepository[model.RatingDetail](db, "rating_detail")
	repoRatingAdjust := repo.NewRepository[model.RatingAdjust](db, "rating_adjust")
	repoRatingTransaction := repo.NewRepository[model.RatingTransaction](db, "rating_transaction")
	repoComment := repo.NewRepository[model.Comment](db, "comment")
	repoCommentAdjust := repo.NewRepository[model.CommentAdjust](db, "comment_adjust")
	repoCommentTransaction := repo.NewRepository[model.CommentTransaction](db, "comment_transaction")
	repoCommentTaxonomy := repo.NewRepository[model.CommentTaxonomy](db, "comment_taxonomy")
	repoEntityTermTaxonomy := repo.NewRepository[model.EntityTermTaxonomy](db, "entity_term_taxonomy")
	repoEntityRelationship := repo.NewRepository[model.EntityRelationship](db, "entity_relationship")
	repoReason := repo.NewRepository[model.Reason](db, "reason")
	repoReasonDetail := repo.NewRepository[model.ReasonDetail](db, "reason_detail")
	repoUser := repo.NewRepository[model.User](db, "user")
	servicePublish := srv.NewPublishingService()

	serviceEntityRelationship := srv.NewEntityRelationshipService(repoEntityRelationship)
	serviceRuleDefault := srv.NewRuleDefaultService(repoRuleDefault, repoEntityRelationship, repoEntityTermTaxonomy, serviceEntityRelationship)
	serviceRatingGroup := srv.NewRatingGroupService(repoRatingGroup)
	serviceRatingDetail := srv.NewRatingDetailService(repoEntityRelationship, repoRatingDetail, repoRatingTransaction, serviceEntityRelationship, serviceRuleDefault)
	serviceRating := srv.NewRatingService(repoRating, repoRatingTransaction, repoRatingAdjust, repoEntityRelationship, repoEntityTermTaxonomy, repoRuleDefault, servicePublish, serviceEntityRelationship, serviceRuleDefault)
	serviceReason := srv.NewReasonService(serviceEntityRelationship, repoReason)
	serviceComment := srv.NewCommentService(repoComment, repoCommentTransaction, repoCommentAdjust, repoCommentTaxonomy, repoReasonDetail, repoRuleDefault, repoUser, servicePublish, serviceRuleDefault)

	handleRuleDefault := handlers.NewRuleDefaultHandlers(serviceRuleDefault)
	handleRating := handlers.NewRatingHandlers(serviceRating)
	handleRatingDetail := handlers.NewRatingDetailHandlers(serviceRatingDetail)
	handleRatingGroup := handlers.NewRatingGroupHandlers(serviceRatingGroup)
	handleComment := handlers.NewCommentHandlers(serviceComment)
	handleReason := handlers.NewReasonHandlers(serviceReason)

	//=========================RuleDefault===============================================
	v1Group.POST("/rule-default/create", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRuleDefault.Create))
	v1Group.GET("/rule-default/get-one/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRuleDefault.GetOne))
	v1Group.PUT("/rule-default/update/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRuleDefault.Update))
	v1Group.PUT("/rule-default/update/entity/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRuleDefault.UpdateRuleEntity))
	v1Group.DELETE("/rule-default/delete/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRuleDefault.Delete))
	v1Group.DELETE("/rule-default/hard-delete/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRuleDefault.DeleteHard))
	v1Group.GET("/rule-default/restore/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRuleDefault.Restore))
	v1Group.GET("/rule-default/get-list", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRuleDefault.List))

	//=========================	RatingGroup ==============================

	v1Group.POST("/rating-group/create", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRatingGroup.Create))
	v1Group.GET("/rating-group/get-one/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRatingGroup.GetOne))
	v1Group.PUT("/rating-group/update/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRatingGroup.Update))
	v1Group.DELETE("/rating-group/delete/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRatingGroup.Delete))
	v1Group.GET("/rating-group/restore/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRatingGroup.Restore))
	v1Group.DELETE("/rating-group/hard-delete/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRatingGroup.DeleteHard))
	v1Group.GET("/rating-group/get-list", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRatingGroup.List))

	//=========================Rating=========================================

	v1Group.POST("/rating/create", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRating.Create))
	v1Group.GET("/rating/get-one/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRating.GetOne))
	v1Group.PUT("/rating/update/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRating.Update))
	v1Group.PUT("/rating/update/entity/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRating.UpdateRatingEntity))
	v1Group.DELETE("/rating/delete/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRating.Delete))
	v1Group.DELETE("/rating/hard-delete/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRating.DeleteHard))
	v1Group.GET("/rating/restore/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRating.Restore))
	v1Group.GET("/rating/get-list", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRating.GetListRating))
	v1Group.GET("/rating/get-list-published", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRating.GetListRatingPublished))

	//=========================RatingDetail=========================================

	v1Group.POST("/rating-detail/create", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRatingDetail.Create))
	v1Group.GET("/rating-detail/get-one/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRatingDetail.GetOne))
	v1Group.PUT("/rating-detail/update/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRatingDetail.Update))
	v1Group.DELETE("/rating-detail/delete/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRatingDetail.Delete))
	v1Group.DELETE("/rating-detail/hard-delete/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRatingDetail.DeleteHard))
	v1Group.GET("/rating-detail/restore/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRatingDetail.Restore))
	v1Group.GET("/rating-detail/get-list", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleRatingDetail.List))

	//=========================Comment=========================================

	v1Group.POST("/comment/create", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleComment.Create))
	v1Group.DELETE("/comment/delete/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleComment.Delete))
	v1Group.DELETE("/comment/hard-delete/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleComment.DeleteHard))
	v1Group.GET("/comment/restore/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleComment.Restore))
	v1Group.PUT("/comment/update-content/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleComment.UpdateContent))
	v1Group.PUT("/comment/update-status/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleComment.UpdateCommentStatus))
	v1Group.PUT("/comment/update/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleComment.Update))
	v1Group.GET("/comment/get-one/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleComment.GetOne))
	v1Group.GET("/comment/get-list", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleComment.List))
	v1Group.GET("/comment/get-list-published", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleComment.GetListCommentPublished))

	//
	////=========================Reason=========================================
	//
	v1Group.POST("/reason/create", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleReason.Create))
	v1Group.GET("/reason/get-list", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleReason.List))
	v1Group.GET("/reason/get-one/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleReason.GetOne))
	v1Group.PUT("/reason/update/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleReason.Update))
	v1Group.PUT("/reason/update/entity/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleReason.UpdateReasonEntity))
	v1Group.DELETE("/reason/delete/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleReason.Delete))
	v1Group.DELETE("/reason/hard-delete/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleReason.DeleteHard))
	v1Group.GET("/reason/restore/:id", ginext.AuthRequiredMiddleware, ginext.WrapHandler(handleReason.Restore))

	//========================= Migrate=========================================

	migrateHandler := handlers.NewMigrationHandler(db)
	v1Group.POST("/internal/migrate", migrateHandler.Migrate)
	if conf.GetConfig().EnvName == "dev" {
		s.Router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	}
	return s
}
