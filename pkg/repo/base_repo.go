package repo

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"strings"
	"time"

	"github.com/samber/lo"

	"meraki-project-template/pkg/model"
	"meraki-project-template/pkg/utils"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

const (
	generalQueryTimeout = 60 * time.Second
)

type Repository[Model interface{}] struct {
	db    *gorm.DB
	table string
}

func NewRepository[Model interface{}](db *gorm.DB, table string) RepositoryInterface[Model] {
	return &Repository[Model]{db, table}
}

type RepositoryInterface[Model interface{}] interface {

	// Filter base repo
	RepoTxn() *gorm.DB
	Filter(tx *gorm.DB, ctx context.Context, f *model.Filter, query model.BaseRepoQueries) (*model.FilterResult[Model], error)
	Find(tx *gorm.DB, ctx context.Context, query model.BaseRepoQueries) ([]*Model, error)
	FindOne(tx *gorm.DB, ctx context.Context, query model.BaseRepoQueries) (*Model, error)
	Create(tx *gorm.DB, ctx context.Context, item *Model) error
	Update(tx *gorm.DB, ctx context.Context, data *Model, query model.BaseRepoQueries) error
	Creates(tx *gorm.DB, ctx context.Context, items []*Model, query model.BaseRepoQueries) ([]*Model, error)
	Updates(tx *gorm.DB, ctx context.Context, item *Model, query model.BaseRepoQueries) ([]*Model, error)
	DeleteAction(tx *gorm.DB, ctx context.Context, req model.DeleteRequest) error
	DeleteUnscope(tx *gorm.DB, ctx context.Context, ids []uuid.UUID) error
}

func (r *Repository[Model]) DBWithTimeout(ctx context.Context) (*gorm.DB, context.CancelFunc) {
	ctx, cancel := context.WithTimeout(ctx, generalQueryTimeout)
	return r.db.WithContext(ctx), cancel
}

func (r *Repository[Model]) RepoTxn() *gorm.DB {
	return r.db
}

func (r *Repository[Model]) Filter(tx *gorm.DB, ctx context.Context, f *model.Filter, query model.BaseRepoQueries) (*model.FilterResult[Model], error) {
	if tx == nil {
		var cancel context.CancelFunc
		tx, cancel = r.DBWithTimeout(ctx)
		defer cancel()
	}

	var table Model
	tx = tx.Model(&table)
	for table, preload := range query.Preloads {
		if table != "" {
			tx = tx.Preload(table, preload...)
		}
	}
	for query, args := range query.Joins {
		if query != "" {
			tx = tx.Joins(query, args...)
		}
	}
	if query.Condition != "" {
		tx = tx.Where(query.Condition, query.Args...)
	}
	result := &model.FilterResult[Model]{
		Filter:  f,
		Records: []*Model{},
	}

	f.Pager.SortableFields = query.SortableFields
	pager := result.Filter.Pager

	tx = pager.DoQuery(&result.Records, tx)
	if tx.Error != nil {
		return nil, tx.Error
	}

	return result, nil
}

func (r *Repository[Model]) Find(tx *gorm.DB, ctx context.Context, query model.BaseRepoQueries) ([]*Model, error) {
	if tx == nil {
		var cancel context.CancelFunc
		tx, cancel = r.DBWithTimeout(ctx)
		defer cancel()
	}
	if query.Condition != "" {
		tx = tx.Where(query.Condition, query.Args...)
	}
	var items []*Model
	err := tx.Find(&items).Error
	return items, err
}

func (r *Repository[Model]) FindOne(tx *gorm.DB, ctx context.Context, query model.BaseRepoQueries) (*Model, error) {
	if tx == nil {
		var cancel context.CancelFunc
		tx, cancel = r.DBWithTimeout(ctx)
		defer cancel()
	}

	var item Model

	for table, preload := range query.Preloads {
		if table != "" {
			tx = tx.Preload(table, preload...)
		}
	}
	for query, args := range query.Joins {
		if query != "" {
			tx = tx.Joins(query, args...)
		}
	}
	if query.Condition != "" {
		tx = tx.Where(query.Condition, query.Args...)
	}
	err := tx.First(&item).Error
	return &item, err
}

func (r *Repository[Model]) Create(tx *gorm.DB, ctx context.Context, item *Model) error {
	if tx == nil {
		var cancel context.CancelFunc
		tx, cancel = r.DBWithTimeout(ctx)
		defer cancel()
	}
	err := tx.Clauses(clause.Returning{}).Create(&item).Error
	return err
}

func (r *Repository[Model]) Update(tx *gorm.DB, ctx context.Context, data *Model, query model.BaseRepoQueries) error {
	if tx == nil {
		var cancel context.CancelFunc
		tx, cancel = r.DBWithTimeout(ctx)
		defer cancel()
	}
	if query.Condition != "" {
		tx = tx.Where(query.Condition, query.Args...)
	}
	err := tx.Clauses(clause.Returning{}).Save(data).Error
	return err
}

func (r *Repository[Model]) Creates(tx *gorm.DB, ctx context.Context, items []*Model, query model.BaseRepoQueries) ([]*Model, error) {
	if tx == nil {
		var cancel context.CancelFunc
		tx, cancel = r.DBWithTimeout(ctx)
		defer cancel()
	}

	chunks := lo.Chunk(items, 1000)
	createdItems := []*Model{}

	for _, chunk := range chunks {
		err := tx.Clauses(query.Clauses...).Create(&chunk).Error
		if err != nil {
			return createdItems, err
		}

		createdItems = append(createdItems, chunk...)
	}

	return createdItems, nil
}

func (r *Repository[Model]) Updates(tx *gorm.DB, ctx context.Context, item *Model, query model.BaseRepoQueries) ([]*Model, error) {
	if tx == nil {
		var cancel context.CancelFunc
		tx, cancel = r.DBWithTimeout(ctx)
		defer cancel()
	}
	var items []*Model
	if query.Condition != "" {
		tx = tx.Where(query.Condition, query.Args...)
	}
	err := tx.Model(&items).Clauses(query.Clauses...).Updates(&item).Error
	return items, err
}

func (r *Repository[Model]) DeleteUnscope(tx *gorm.DB, ctx context.Context, ids []uuid.UUID) error {
	if tx == nil {
		var cancel context.CancelFunc
		tx, cancel = r.DBWithTimeout(ctx)
		defer cancel()
	}
	var table Model
	return tx.Unscoped().Where("ID in (?)", ids).Delete(table).Error
}

func (r *Repository[Model]) DeleteAction(tx *gorm.DB, ctx context.Context, req model.DeleteRequest) error {
	if tx == nil {
		var cancel context.CancelFunc
		tx, cancel = r.DBWithTimeout(ctx)
		defer cancel()
	}

	// get invalid id from request lists
	var table Model
	if err := r.ValidDeleteIDs(tx, req); err != nil {
		return err
	}
	switch req.Action {
	case utils.IS_DELETE_TRASH:
		return HandleDeleteAction(tx, req.Action).Model(table).Where("id IN ?", req.ID).
			Updates(map[string]interface{}{
				utils.SQL_DELETED_AT: gorm.Expr("CURRENT_TIMESTAMP"),
				utils.SQL_UPDATER_ID: *req.CurrentUser,
			}).Error
	case utils.IS_DELETE_HARD_TRASH:
		return HandleDeleteAction(tx, req.Action).Model(table).Where("id IN ?", req.ID).
			Updates(map[string]interface{}{
				utils.SQL_DELETED_AT:      gorm.Expr("CURRENT_TIMESTAMP"),
				utils.SQL_HARD_DELETED_AT: gorm.Expr("CURRENT_TIMESTAMP"),
				utils.SQL_UPDATER_ID:      *req.CurrentUser,
			}).Error
	case utils.REVERT:
		return HandleDeleteAction(tx, req.Action).Model(table).Where("id IN ?", req.ID).
			Updates(map[string]interface{}{
				utils.SQL_DELETED_AT:      gorm.Expr("NULL"),
				utils.SQL_HARD_DELETED_AT: gorm.Expr("NULL"),
				utils.SQL_UPDATER_ID:      *req.CurrentUser,
			}).Error
	default:
		return fmt.Errorf("invalid delete action (switch): %v", req.Action)
	}
}

func (r *Repository[Model]) ValidDeleteIDs(tx *gorm.DB, req model.DeleteRequest) error {
	var table Model
	var missingIdList []string
	var subQuery1Tmp []string
	for _, v := range req.ID {
		subQuery1Tmp = append(subQuery1Tmp, fmt.Sprintf("SELECT '%v' AS id", v))
	}

	subQuery1 := tx.Raw(strings.Join(subQuery1Tmp, " UNION ALL "))
	subQuery2 := HandleDeleteAction(tx, req.Action).Select("id").Model(&table).Where("id IN ?", req.ID)

	if req.Action == utils.IS_DELETE_HARD_TRASH || req.Action == utils.REVERT {
		tx = tx.Unscoped()
	}

	if err := tx.Table("(?) as id_list", subQuery1).Where("CAST(id AS uuid) NOT IN (?)", subQuery2).Pluck("id", &missingIdList).Error; err != nil {
		return fmt.Errorf("error getting missing id from delete request: %v", err)
	}

	if len(missingIdList) > 0 {
		return fmt.Errorf("record not found: id %v not exist", strings.Join(missingIdList, ", "))
	}

	return nil
}

func HandleDeleteAction(tx *gorm.DB, action string) *gorm.DB {
	switch action {
	case utils.IS_DELETE_TRASH:
		return tx.Unscoped().Where("? IS NULL", gorm.Expr(utils.SQL_DELETED_AT))
	case utils.IS_DELETE_HARD_TRASH:
		return tx.Unscoped().Where("? IS NULL OR ? IS NULL", gorm.Expr(utils.SQL_DELETED_AT), gorm.Expr(utils.SQL_HARD_DELETED_AT))
	case utils.REVERT:
		return tx.Unscoped().Where("? IS NOT NULL OR ? IS NOT NULL", gorm.Expr(utils.SQL_DELETED_AT), gorm.Expr(utils.SQL_HARD_DELETED_AT))
	}
	return tx
}
