package handlers

import (
	"gitlab.com/merakilab9/common/ginext"
	_ "gitlab.com/merakilab9/common/ginext"
	"gitlab.com/merakilab9/common/lib"
	"gitlab.com/merakilab9/common/logger"
	"net/http"

	"github.com/google/uuid"

	"meraki-project-template/pkg/model"
	"meraki-project-template/pkg/service"
	"meraki-project-template/pkg/utils"
)

type RatingHandlers struct {
	ratingService service.RatingInterface
}

func NewRatingHandlers(ratingService service.RatingInterface) *RatingHandlers {
	return &RatingHandlers{ratingService: ratingService}
}

// Create
// @Tags Rating
// @Title Rating Create
// @Description Create a rating group
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param   rating body model.RatingRequest true "New Rating"
// @Success 200 {object} model.Rating
// @Success 201 {object} model.Rating
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating/create [post]
func (h *RatingHandlers) Create(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	owner, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	req := model.RatingRequest{}
	if err := r.GinCtx.BindJSON(&req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	if err = lib.CheckRequireValid(req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	ob, err := h.ratingService.CreateRating(r.Context(), &req, owner)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// GetOne
// @Tags Rating
// @Title Rating GetOne
// @Description get item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	id  path 	string	true "id item"
// @Success 200 {object} model.Rating
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating/get-one/{id} [get]
func (h *RatingHandlers) GetOne(r *ginext.Request) (*ginext.Response, error) {
	ID := &uuid.UUID{}
	if ID = utils.ParseIDFromUri(r.GinCtx); ID == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	ob, err := h.ratingService.GetOneRating(r.Context(), *ID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_NOT_FOUND, err.Error())
	}
	r.MustNoError(err)

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// Update
// @Tags Rating
// @Title Rating Update
// @Description update item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Param   rating body model.RatingRequest true "New Rating"
// @Success 200 {object} ginext.Response //run "swag init --parseDependency --parseInternal -g pkg/service/service.go"
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating/update/{id} [put]
func (h *RatingHandlers) Update(r *ginext.Request) (*ginext.Response, error) {
	l := logger.WithCtx(r.Context(), "RatingHandlers.Update")
	req := model.RatingRequest{}
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	strEntityTaxonomyID := ginext.GetEntityTaxonomyID(r.GinCtx)
	entityTaxonomyID, err := uuid.Parse(strEntityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	if req.ID = utils.ParseIDFromUri(r.GinCtx); req.ID == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	if err := r.GinCtx.BindJSON(&req); err != nil {
		l.WithError(err).WithField("todo.id", req.ID).Error("failed to query item")
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}
	//if err = lib.CheckRequireValid(req); err != nil {
	//	return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	//}

	ob, err := h.ratingService.UpdateRating(r.Context(), &req, currentUser, entityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// UpdateRatingEntity
// @Tags UpdateRatingEntity
// @Title UpdateRatingEntity
// @Description update item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Param   rating body model.RatingRequest true "New Rating"
// @Success 200 {object} ginext.Response //run "swag init --parseDependency --parseInternal -g pkg/service/service.go"
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating/update/entity/{id} [put]
func (h *RatingHandlers) UpdateRatingEntity(r *ginext.Request) (*ginext.Response, error) {
	l := logger.WithCtx(r.Context(), "RatingHandlers.Update")
	req := model.UpdateEntityRequest{}
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	strEntityTaxonomyID := ginext.GetEntityTaxonomyID(r.GinCtx)
	entityTaxonomyID, err := uuid.Parse(strEntityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	if req.ID = utils.ParseIDFromUri(r.GinCtx); req.ID == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	if err := r.GinCtx.BindJSON(&req); err != nil {
		l.WithError(err).WithField("todo.id", req.ID).Error("failed to query item")
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	if err = lib.CheckRequireValid(req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}
	ob, err := h.ratingService.UpdateRatingEntity(r.Context(), &req, currentUser, entityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// Delete
// @Tags Rating
// @Title Rating Delete
// @Description delete item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Success 200 {object} ginext.Response
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating/delete/{id} [delete]
func (h *RatingHandlers) Delete(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	strEntityTaxonomyID := ginext.GetEntityTaxonomyID(r.GinCtx)
	entityTaxonomyID, err := uuid.Parse(strEntityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	id := utils.ParseIDFromUri(r.GinCtx)
	if id == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	req := model.DeleteRequest{
		ID:          []uuid.UUID{*id},
		Action:      lib.IS_DELETE_TRASH,
		CurrentUser: &currentUser,
	}

	if err := h.ratingService.DeleteRating(r.Context(), req, entityTaxonomyID); err != nil {
		return nil, ginext.NewError(lib.CODE_NOT_FOUND, err.Error())
	}

	return ginext.NewResponse(http.StatusOK), nil
}

// DeleteHard
// @Tags Rating
// @Title Rating DeleteHard
// @Description delete item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Success 200 {object} ginext.Response
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating/delete-hard/{id} [delete]
func (h *RatingHandlers) DeleteHard(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	strEntityTaxonomyID := ginext.GetEntityTaxonomyID(r.GinCtx)
	entityTaxonomyID, err := uuid.Parse(strEntityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	id := utils.ParseIDFromUri(r.GinCtx)
	if id == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	req := model.DeleteRequest{
		ID:          []uuid.UUID{*id},
		Action:      lib.IS_DELETE_HARD_TRASH,
		CurrentUser: &currentUser,
	}
	r.MustNoError(h.ratingService.DeleteRating(r.Context(), req, entityTaxonomyID))

	return ginext.NewResponse(http.StatusOK), nil
}

// Restore
// @Tags Rating
// @Title Rating Restore
// @Description delete item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Success 200 {object} ginext.Response
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating/delete/{id} [delete]
func (h *RatingHandlers) Restore(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	strEntityTaxonomyID := ginext.GetEntityTaxonomyID(r.GinCtx)
	entityTaxonomyID, err := uuid.Parse(strEntityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	id := utils.ParseIDFromUri(r.GinCtx)
	if id == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	req := model.DeleteRequest{
		ID:          []uuid.UUID{*id},
		Action:      lib.REVERT,
		CurrentUser: &currentUser,
	}
	r.MustNoError(h.ratingService.DeleteRating(r.Context(), req, entityTaxonomyID))
	return ginext.NewResponse(http.StatusOK), nil
}

// GetListRating List
// @Tags Rating
// @Title Rating List
// @Description Rating List
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	page_size	query	int	true	"size per page"
// @Param	page	    query	int	true	"page number (> 0)"
// @Param	creator_id	query	string	false	"creator_id"
// @Param	sort	    query	string	false	"sort"
// @Param	name	    query	string	false	"name"
// @Param	key	        query	string	false	"key"
// @Param	entity_term_taxonomy_id	    query	string	false	"entity_term_taxonomy_id"
// @Param	description	query	string	false	"description"
// @Param	is_active	query	string	false	"is_active"
// @Success 200 {object} []model.Rating
// @Failure 500 {object} ginext.Response
// @Router /rating/owner/get-list [get]
func (h *RatingHandlers) GetListRating(r *ginext.Request) (*ginext.Response, error) {
	req := model.RatingGetListRequest{}
	strUserID := ginext.GetUserID(r.GinCtx)
	owner, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	if err := r.GinCtx.BindQuery(&req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	if err = lib.CheckRequireValid(req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	req.CurrentUser = &owner
	filter := &model.Filter{
		Request: req,
		Pager:   ginext.NewPagerWithGinCtx(r.GinCtx),
	}

	result, err := h.ratingService.GetListRating(r.Context(), filter)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}
	r.MustNoError(err)

	return ginext.NewResponseWithPager(http.StatusOK, result.Records, result.Filter.Pager), nil
}

// GetListRatingPublished List
// @Tags Rating
// @Title Rating List
// @Description Rating List
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	page_size	query	int	true	"size per page"
// @Param	page	    query	int	true	"page number (> 0)"
// @Param	creator_id	query	string	false	"creator_id"
// @Param	sort	    query	string	false	"sort"
// @Param	name	    query	string	false	"name"
// @Param	key	        query	string	false	"key"
// @Param	entity_term_taxonomy_id	    query	string	false	"entity_term_taxonomy_id"
// @Param	description	query	string	false	"description"
// @Param	is_active	query	string	false	"is_active"
// @Success 200 {object} []model.Rating
// @Failure 500 {object} ginext.Response
// @Router /rating/users/get-list-published [get]
func (h *RatingHandlers) GetListRatingPublished(r *ginext.Request) (*ginext.Response, error) {
	req := model.RatingGetListRequest{}

	if err := r.GinCtx.BindQuery(&req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	if err := lib.CheckRequireValid(req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	filter := &model.Filter{
		Request: req,
		Pager:   ginext.NewPagerWithGinCtx(r.GinCtx),
	}

	result, err := h.ratingService.GetListRatingPublished(r.Context(), filter)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}
	r.MustNoError(err)

	return ginext.NewResponseWithPager(http.StatusOK, result.Records, result.Filter.Pager), nil
}
