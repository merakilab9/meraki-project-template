package handlers

import (
	"gitlab.com/merakilab9/common/ginext"
	_ "gitlab.com/merakilab9/common/ginext"
	"gitlab.com/merakilab9/common/lib"
	"gitlab.com/merakilab9/common/logger"
	"net/http"

	"github.com/google/uuid"

	"meraki-project-template/pkg/model"
	"meraki-project-template/pkg/service"
	"meraki-project-template/pkg/utils"
)

type ReasonHandlers struct {
	ReasonService service.ReasonInterface
}

func NewReasonHandlers(ReasonService service.ReasonInterface) *ReasonHandlers {
	return &ReasonHandlers{ReasonService: ReasonService}
}

// Create
// @Tags Reason
// @Title Reason Create
// @Description Create a Reason group
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param   Reason body model.ReasonRequest true "New Reason"
// @Success 200 {object} model.Reason
// @Success 201 {object} model.Reason
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /reason/create [post]
func (h *ReasonHandlers) Create(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	owner, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	req := model.ReasonRequest{}
	if err := r.GinCtx.BindJSON(&req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	if err = lib.CheckRequireValid(req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	ob, err := h.ReasonService.CreateReason(r.Context(), &req, owner)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// List
// @Tags Reason
// @Title Reason List
// @Description Reason List
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	page_size	query	int	true	"size per page"
// @Param	page	    query	int	true	"page number (> 0)"
// @Param	creator_id	query	string	false	"creator_id"
// @Param	sort	    query	string	false	"sort"
// @Param	name	    query	string	false	"name"
// @Param	key	        query	string	false	"key"
// @Param	group	    query	string	false	"group"
// @Param	description	query	string	false	"description"
// @Param	is_active	query	string	false	"is_active"
// @Success 200 {object} []model.Reason
// @Failure 500 {object} ginext.Response
// @Router /reason/get-list [get]
func (h *ReasonHandlers) List(r *ginext.Request) (*ginext.Response, error) {
	req := model.ReasonListRequest{}

	if err := r.GinCtx.BindQuery(&req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	filter := &model.Filter{
		Request: req,
		Pager:   ginext.NewPagerWithGinCtx(r.GinCtx),
	}

	if err := lib.CheckRequireValid(req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	result, err := h.ReasonService.FilterReason(r.Context(), filter)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}
	r.MustNoError(err)

	return ginext.NewResponseWithPager(http.StatusOK, result.Records, result.Filter.Pager), nil
}

// GetOne
// @Tags Reason
// @Title Reason GetOne
// @Description get item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	id  path 	string	true "id item"
// @Success 200 {object} model.Reason
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /reason/get-one/{id} [get]
func (h *ReasonHandlers) GetOne(r *ginext.Request) (*ginext.Response, error) {
	ID := &uuid.UUID{}
	if ID = utils.ParseIDFromUri(r.GinCtx); ID == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	ob, err := h.ReasonService.GetOneReason(r.Context(), *ID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_NOT_FOUND, err.Error())
	}
	r.MustNoError(err)

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// Update
// @Tags Reason
// @Title Reason Update
// @Description update item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Param   Reason body model.ReasonRequest true "New Reason"
// @Success 200 {object} ginext.Response //run "swag init --parseDependency --parseInternal -g pkg/service/service.go"
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /reason/update/{id} [put]
func (h *ReasonHandlers) Update(r *ginext.Request) (*ginext.Response, error) {
	l := logger.WithCtx(r.Context(), "ReasonHandlers.Update")
	req := model.ReasonRequest{}
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	if req.ID = utils.ParseIDFromUri(r.GinCtx); req.ID == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	if err := r.GinCtx.BindJSON(&req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	ob, err := h.ReasonService.GetOneReason(r.Context(), *req.ID)
	if err != nil {
		l.WithError(err).WithField("todo.id", req.ID).Error("failed to query item")
		return nil, ginext.NewError(lib.CODE_NOT_FOUND, err.Error())
	}

	ob.UpdaterID = &currentUser
	lib.Sync(req, ob)

	if err = h.ReasonService.UpdateReason(r.Context(), ob); err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// UpdateReasonEntity
// @Tags UpdateReasonEntity
// @Title UpdateReasonEntity
// @Description update item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Param   Reason body model.ReasonRequest true "New Reason"
// @Success 200 {object} ginext.Response //run "swag init --parseDependency --parseInternal -g pkg/service/service.go"
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /reason/update/entity/{id} [put]
func (h *ReasonHandlers) UpdateReasonEntity(r *ginext.Request) (*ginext.Response, error) {
	l := logger.WithCtx(r.Context(), "ReasonHandlers.Update")
	req := model.UpdateEntityRequest{}
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	//strEntityTaxonomyID := ginext.GetEntityTaxonomyID(r.GinCtx)
	//entityTaxonomyID, err := uuid.Parse(strEntityTaxonomyID)
	//if err != nil {
	//	return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	//}

	if req.ID = utils.ParseIDFromUri(r.GinCtx); req.ID == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	if err := r.GinCtx.BindJSON(&req); err != nil {
		l.WithError(err).WithField("todo.id", req.ID).Error("failed to query item")
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	if err = lib.CheckRequireValid(req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}
	ob, err := h.ReasonService.UpdateReasonEntity(r.Context(), &req, currentUser, uuid.Nil)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// Delete
// @Tags Reason
// @Title Reason Delete
// @Description delete item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Success 200 {object} ginext.Response
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /reason/delete/{id} [delete]
func (h *ReasonHandlers) Delete(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	id := utils.ParseIDFromUri(r.GinCtx)
	if id == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	req := model.DeleteRequest{
		ID:          []uuid.UUID{*id},
		Action:      lib.IS_DELETE_TRASH,
		CurrentUser: &currentUser,
	}

	if err := h.ReasonService.DeleteReason(r.Context(), req); err != nil {
		return nil, ginext.NewError(lib.CODE_NOT_FOUND, err.Error())
	}

	return ginext.NewResponse(http.StatusOK), nil
}

// DeleteHard
// @Tags Reason
// @Title Reason DeleteHard
// @Description delete item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Success 200 {object} ginext.Response
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /reason/delete-hard/{id} [delete]
func (h *ReasonHandlers) DeleteHard(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	id := utils.ParseIDFromUri(r.GinCtx)
	if id == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	req := model.DeleteRequest{
		ID:          []uuid.UUID{*id},
		Action:      lib.IS_DELETE_HARD_TRASH,
		CurrentUser: &currentUser,
	}
	r.MustNoError(h.ReasonService.DeleteReason(r.Context(), req))

	return ginext.NewResponse(http.StatusOK), nil
}

// Restore
// @Tags Reason
// @Title Reason Restore
// @Description delete item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Success 200 {object} ginext.Response
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /reason/delete/{id} [delete]
func (h *ReasonHandlers) Restore(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	id := utils.ParseIDFromUri(r.GinCtx)
	if id == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	req := model.DeleteRequest{
		ID:          []uuid.UUID{*id},
		Action:      lib.REVERT,
		CurrentUser: &currentUser,
	}
	r.MustNoError(h.ReasonService.DeleteReason(r.Context(), req))
	return ginext.NewResponse(http.StatusOK), nil
}
