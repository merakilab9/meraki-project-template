package handlers

import (
	"gitlab.com/merakilab9/common/ginext"
	_ "gitlab.com/merakilab9/common/ginext"
	"gitlab.com/merakilab9/common/lib"
	"gitlab.com/merakilab9/common/logger"
	"net/http"

	"github.com/google/uuid"

	"meraki-project-template/pkg/model"
	"meraki-project-template/pkg/service"
	"meraki-project-template/pkg/utils"
)

type RatingDetailHandlers struct {
	RatingDetailService service.RatingDetailInterface
}

func NewRatingDetailHandlers(RatingDetailService service.RatingDetailInterface) *RatingDetailHandlers {
	return &RatingDetailHandlers{RatingDetailService: RatingDetailService}
}

// Create
// @Tags RatingDetail
// @Title RatingDetail Create
// @Description Create a RatingDetail group
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param   RatingDetail body model.RatingDetailRequest true "New RatingDetail"
// @Success 200 {object} model.RatingDetail
// @Success 201 {object} model.RatingDetail
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating-detail/create [post]
func (h *RatingDetailHandlers) Create(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	owner, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	strEntityTaxonomyID := ginext.GetEntityTaxonomyID(r.GinCtx)
	entityTaxonomyID, err := uuid.Parse(strEntityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	req := model.RatingDetailRequest{}
	if err := r.GinCtx.BindJSON(&req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	if err = lib.CheckRequireValid(req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	ob, err := h.RatingDetailService.CreateRatingDetail(r.Context(), &req, &owner, &entityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// List
// @Tags RatingDetail
// @Title RatingDetail List
// @Description RatingDetail List
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	page_size	query	int	true	"size per page"
// @Param	page	    query	int	true	"page number (> 0)"
// @Param	creator_id	query	string	false	"creator_id"
// @Param	sort	    query	string	false	"sort"
// @Param	name	    query	string	false	"name"
// @Param	key	        query	string	false	"key"
// @Param	group	    query	string	false	"group"
// @Param	description	query	string	false	"description"
// @Param	is_active	query	string	false	"is_active"
// @Success 200 {object} []model.RatingDetail
// @Failure 500 {object} ginext.Response
// @Router /rating-detail/get-list [get]
func (h *RatingDetailHandlers) List(r *ginext.Request) (*ginext.Response, error) {
	req := model.RatingDetailListRequest{}

	if err := r.GinCtx.BindQuery(&req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	filter := &model.Filter{
		Request: req,
		Pager:   ginext.NewPagerWithGinCtx(r.GinCtx),
	}

	result, err := h.RatingDetailService.FilterRatingDetail(r.Context(), filter)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}
	r.MustNoError(err)

	return ginext.NewResponseWithPager(http.StatusOK, result.Records, result.Filter.Pager), nil
}

// GetOne
// @Tags RatingDetail
// @Title RatingDetail GetOne
// @Description get item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	id  path 	string	true "id item"
// @Success 200 {object} model.RatingDetail
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating-detail/get-one/{id} [get]
func (h *RatingDetailHandlers) GetOne(r *ginext.Request) (*ginext.Response, error) {
	ID := &uuid.UUID{}
	if ID = utils.ParseIDFromUri(r.GinCtx); ID == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	ob, err := h.RatingDetailService.GetOneRatingDetail(r.Context(), *ID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_NOT_FOUND, err.Error())
	}
	r.MustNoError(err)

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// Update
// @Tags RatingDetail
// @Title RatingDetail Update
// @Description update item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Param   RatingDetail body model.RatingDetailRequest true "New RatingDetail"
// @Success 200 {object} ginext.Response //run "swag init --parseDependency --parseInternal -g pkg/service/service.go"
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating-detail/update/{id} [put]
func (h *RatingDetailHandlers) Update(r *ginext.Request) (*ginext.Response, error) {
	l := logger.WithCtx(r.Context(), "RatingDetailHandlers.Update")
	req := model.UpdateRatingDetailRequest{}
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	if req.ID = utils.ParseIDFromUri(r.GinCtx); req.ID == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	if err := r.GinCtx.BindJSON(&req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	ob, err := h.RatingDetailService.GetOneRatingDetail(r.Context(), *req.ID)
	if err != nil {
		l.WithError(err).WithField("todo.id", req.ID).Error("failed to query item")
		return nil, ginext.NewError(lib.CODE_NOT_FOUND, err.Error())
	}

	ob.UpdaterID = &currentUser
	lib.Sync(req, ob)

	if err = h.RatingDetailService.UpdateRatingDetail(r.Context(), ob); err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// Delete
// @Tags RatingDetail
// @Title RatingDetail Delete
// @Description delete item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Success 200 {object} ginext.Response
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating-detail/delete/{id} [delete]
func (h *RatingDetailHandlers) Delete(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	id := utils.ParseIDFromUri(r.GinCtx)
	if id == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	req := model.DeleteRequest{
		ID:          []uuid.UUID{*id},
		Action:      lib.IS_DELETE_TRASH,
		CurrentUser: &currentUser,
	}
	r.MustNoError(h.RatingDetailService.DeleteRatingDetail(r.Context(), req))

	return ginext.NewResponse(http.StatusOK), nil
}

// Restore
// @Tags RatingDetail
// @Title RatingDetail Restore
// @Description delete item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Success 200 {object} ginext.Response
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating-detail/delete/{id} [delete]
func (h *RatingDetailHandlers) Restore(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	id := utils.ParseIDFromUri(r.GinCtx)
	if id == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	req := model.DeleteRequest{
		ID:          []uuid.UUID{*id},
		Action:      lib.REVERT,
		CurrentUser: &currentUser,
	}
	r.MustNoError(h.RatingDetailService.DeleteRatingDetail(r.Context(), req))

	return ginext.NewResponse(http.StatusOK), nil
}

// DeleteHard
// @Tags RatingDetail
// @Title RatingDetail DeleteHard
// @Description delete item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Success 200 {object} ginext.Response
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating-detail/delete-hard/{id} [delete]
func (h *RatingDetailHandlers) DeleteHard(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	id := utils.ParseIDFromUri(r.GinCtx)
	if id == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	req := model.DeleteRequest{
		ID:          []uuid.UUID{*id},
		Action:      lib.IS_DELETE_HARD_TRASH,
		CurrentUser: &currentUser,
	}
	r.MustNoError(h.RatingDetailService.DeleteRatingDetail(r.Context(), req))

	return ginext.NewResponse(http.StatusOK), nil
}
