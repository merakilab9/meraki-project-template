package handlers

import (
	"gitlab.com/merakilab9/common/ginext"
	_ "gitlab.com/merakilab9/common/ginext"
	"gitlab.com/merakilab9/common/lib"
	"gitlab.com/merakilab9/common/logger"
	"meraki-project-template/pkg/utils"
	"net/http"

	"github.com/google/uuid"

	"meraki-project-template/pkg/model"
	"meraki-project-template/pkg/service"
)

type CommentHandlers struct {
	CommentService service.CommentInterface
}

func NewCommentHandlers(commentService service.CommentInterface) *CommentHandlers {
	return &CommentHandlers{CommentService: commentService}
}

// Create
// @Tags Comment
// @Title Comment Create
// @Description Create a comment
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param   comment body model.CommentRequest true "New Comment"
// @Success 200 {object} model.Comment
// @Success 201 {object} model.Comment
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /comment/create [post]
func (h *CommentHandlers) Create(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	owner, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	strEntityTaxonomyID := ginext.GetEntityTaxonomyID(r.GinCtx)
	entityTaxonomyID, err := uuid.Parse(strEntityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	req := model.CommentRequest{}
	if err := r.GinCtx.BindJSON(&req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	if err = lib.CheckRequireValid(req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	ob, err := h.CommentService.CreateComment(r.Context(), &req, owner, entityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// List
// @Tags Comment
// @Title Comment List
// @Description Comment List
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	page_size	query	int	true	"size per page"
// @Param	page	    query	int	true	"page number (> 0)"
// @Param	creator_id	query	string	false	"creator_id"
// @Param	sort	    query	string	false	"sort"
// @Param	name	    query	string	false	"name"
// @Param	key	        query	string	false	"key"
// @Param	group	    query	string	false	"group"
// @Param	description	query	string	false	"description"
// @Param	is_active	query	string	false	"is_active"
// @Success 200 {object} []model.Comment
// @Failure 500 {object} ginext.Response
// @Router /comment/get-list [get]
func (h *CommentHandlers) List(r *ginext.Request) (*ginext.Response, error) {
	req := model.CommentListRequest{}
	if err := r.GinCtx.BindQuery(&req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}
	filter := &model.Filter{
		Request: req,
		Pager:   ginext.NewPagerWithGinCtx(r.GinCtx),
	}
	result, err := h.CommentService.FilterComment(r.Context(), filter)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}
	r.MustNoError(err)
	return ginext.NewResponseWithPager(http.StatusOK, result.Records, result.Filter.Pager), nil
}

// GetListCommentPublished
// @Tags Comment
// @Title Comment GetListCommentPublished
// @Description Comment List
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	page_size	query	int	true	"size per page"
// @Param	page	    query	int	true	"page number (> 0)"
// @Param	creator_id	query	string	false	"creator_id"
// @Param	sort	    query	string	false	"sort"
// @Param	object_id	query	string	false	"object_id"
// @Param	parent_id	query	string	false	"parent_id"
// @Param	status	    query	string	false	"status"
// @Success 200 {object} []model.Comment
// @Failure 500 {object} ginext.Response
// @Router /comment/get-list-published [get]

func (h *CommentHandlers) GetListCommentPublished(r *ginext.Request) (*ginext.Response, error) {
	req := model.CommentListRequest{}
	r.MustBind(&req)

	filter := &model.Filter{
		Request: req,
		Pager:   ginext.NewPagerWithGinCtx(r.GinCtx),
	}

	result, err := h.CommentService.GetListCommentPublished(r.Context(), filter)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}
	r.MustNoError(err)

	return ginext.NewResponseWithPager(http.StatusOK, result.Records, result.Filter.Pager), nil
}

// GetOne
// @Tags Comment
// @Title Comment GetOne
// @Description get item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	id  path 	string	true "id item"
// @Success 200 {object} model.Comment
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /comment/get-one/{id} [get]

func (h *CommentHandlers) GetOne(r *ginext.Request) (*ginext.Response, error) {
	ID := &uuid.UUID{}
	if ID = utils.ParseIDFromUri(r.GinCtx); ID == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	ob, err := h.CommentService.GetOneComment(r.Context(), *ID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_NOT_FOUND, err.Error())
	}
	r.MustNoError(err)

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// UpdateContent
// @Tags Comment
// @Title Comment Update
// @Description update item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Param   Comment body model.CommentRequest true "New Comment"
// @Success 200 {object} ginext.Response //run "swag init --parseDependency --parseInternal -g pkg/service/service.go"
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /comment/update-content/{id} [put]
func (h *CommentHandlers) UpdateContent(r *ginext.Request) (*ginext.Response, error) {
	req := model.UpdateCommentContentRequest{}
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	strEntityTaxonomyID := ginext.GetEntityTaxonomyID(r.GinCtx)
	entityTaxonomyID, err := uuid.Parse(strEntityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	if req.ID = utils.ParseIDFromUri(r.GinCtx); req.ID == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	if err := r.GinCtx.BindJSON(&req); err != nil {

		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	if err = lib.CheckRequireValid(req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	ob, err := h.CommentService.UpdateCommentContent(r.Context(), &req, currentUser, entityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// Update
// @Tags Comment
// @Title Update
// @Description update item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Param   Comment body model.CommentRequest true "New Comment"
// @Success 200 {object} ginext.Response //run "swag init --parseDependency --parseInternal -g pkg/service/service.go"
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /comment/update/{id} [put]
func (h *CommentHandlers) Update(r *ginext.Request) (*ginext.Response, error) {
	l := logger.WithCtx(r.Context(), "RatingHandlers.Update")
	req := model.UpdateCommentRequest{}
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	strEntityTaxonomyID := ginext.GetEntityTaxonomyID(r.GinCtx)
	entityTaxonomyID, err := uuid.Parse(strEntityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	if req.ID = utils.ParseIDFromUri(r.GinCtx); req.ID == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	if err := r.GinCtx.BindJSON(&req); err != nil {
		l.WithError(err).WithField("todo.id", req.ID).Error("failed to query item")
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	ob, err := h.CommentService.UpdateComment(r.Context(), &req, currentUser, entityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// UpdateCommentStatus
// @Tags Comment
// @Title UpdateCommentStatus
// @Description update item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	action		path 	string	true "action item"
// @Param   Comment body model.UpdateTransactionCommentRequest true "New Comment"
// @Success 200 {object} ginext.Response //run "swag init --parseDependency --parseInternal -g pkg/service/service.go"
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /comment/update-status/{id} [put]
func (h *CommentHandlers) UpdateCommentStatus(r *ginext.Request) (*ginext.Response, error) {
	req := model.UpdateTransactionCommentRequest{}
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	strEntityTaxonomyID := ginext.GetEntityTaxonomyID(r.GinCtx)
	entityTaxonomyID, err := uuid.Parse(strEntityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	if err := r.GinCtx.BindJSON(&req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	if err = lib.CheckRequireValid(req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	id := utils.ParseIDFromUri(r.GinCtx)
	if id == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	req.ID = id
	ob, err := h.CommentService.UpdateCommentStatus(r.Context(), &req, currentUser, entityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// Delete
// @Tags Comment
// @Title Comment Delete
// @Description delete item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Success 200 {object} ginext.Response
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /comment/delete/{id} [delete]
func (h *CommentHandlers) Delete(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	strEntityTaxonomyID := ginext.GetEntityTaxonomyID(r.GinCtx)
	entityTaxonomyID, err := uuid.Parse(strEntityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	id := utils.ParseIDFromUri(r.GinCtx)
	if id == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	req := model.DeleteRequest{
		ID:          []uuid.UUID{*id},
		Action:      lib.IS_DELETE_TRASH,
		CurrentUser: &currentUser,
	}

	if err := h.CommentService.DeleteComment(r.Context(), req, entityTaxonomyID); err != nil {
		return nil, ginext.NewError(lib.CODE_NOT_FOUND, err.Error())
	}

	return ginext.NewResponse(http.StatusOK), nil
}

// DeleteHard
// @Tags Comment
// @Title Comment DeleteHard
// @Description delete item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Success 200 {object} ginext.Response
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /comment/delete-hard/{id} [delete]
func (h *CommentHandlers) DeleteHard(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	strEntityTaxonomyID := ginext.GetEntityTaxonomyID(r.GinCtx)
	entityTaxonomyID, err := uuid.Parse(strEntityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	id := utils.ParseIDFromUri(r.GinCtx)
	if id == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	req := model.DeleteRequest{
		ID:          []uuid.UUID{*id},
		Action:      lib.IS_DELETE_HARD_TRASH,
		CurrentUser: &currentUser,
	}
	r.MustNoError(h.CommentService.DeleteComment(r.Context(), req, entityTaxonomyID))

	return ginext.NewResponse(http.StatusOK), nil
}

// Restore
// @Tags Comment
// @Title Comment Restore
// @Description delete item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Success 200 {object} ginext.Response
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /comment/delete/{id} [delete]
func (h *CommentHandlers) Restore(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	strEntityTaxonomyID := ginext.GetEntityTaxonomyID(r.GinCtx)
	entityTaxonomyID, err := uuid.Parse(strEntityTaxonomyID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	id := utils.ParseIDFromUri(r.GinCtx)
	if id == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	req := model.DeleteRequest{
		ID:          []uuid.UUID{*id},
		Action:      lib.REVERT,
		CurrentUser: &currentUser,
	}
	r.MustNoError(h.CommentService.DeleteComment(r.Context(), req, entityTaxonomyID))
	return ginext.NewResponse(http.StatusOK), nil
}
