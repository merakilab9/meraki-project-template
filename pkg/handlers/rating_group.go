package handlers

import (
	"gitlab.com/merakilab9/common/ginext"
	_ "gitlab.com/merakilab9/common/ginext"
	"gitlab.com/merakilab9/common/lib"
	"gitlab.com/merakilab9/common/logger"
	"net/http"

	"github.com/google/uuid"

	"meraki-project-template/pkg/model"
	"meraki-project-template/pkg/service"
	"meraki-project-template/pkg/utils"
)

type RatingGroupHandlers struct {
	ratingGroupService service.RatingGroupInterface
}

func NewRatingGroupHandlers(ratingGroupService service.RatingGroupInterface) *RatingGroupHandlers {
	return &RatingGroupHandlers{ratingGroupService: ratingGroupService}
}

// Create
// @Tags RatingGroup
// @Title Rating Group Create
// @Description Create a rating group
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param   rating_group body model.RatingGroupRequest true "New RatingGroup"
// @Success 200 {object} model.RatingGroup
// @Success 201 {object} model.RatingGroup
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating-group/create [post]
func (h *RatingGroupHandlers) Create(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	owner, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	req := model.RatingGroupRequest{}
	if err := r.GinCtx.BindJSON(&req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}
	if err = lib.CheckRequireValid(req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	ob := &model.RatingGroup{
		BaseModel: model.BaseModel{
			CreatorID: &owner,
		},
	}
	lib.Sync(req, ob)
	if err = h.ratingGroupService.CreateRatingGroup(r.Context(), ob); err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// List
// @Tags RatingGroup
// @Title Rating Group List
// @Description RatingGroup List
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	page_size	query	int	true	"size per page"
// @Param	page	    query	int	true	"page number (> 0)"
// @Param	creator_id	query	string	false	"creator_id"
// @Param	sort	    query	string	false	"sort"
// @Param	name	    query	string	false	"name"
// @Param	key	        query	string	false	"key"
// @Param	group	    query	string	false	"group"
// @Param	description	query	string	false	"description"
// @Param	is_active	query	string	false	"is_active"
// @Success 200 {object} []model.RatingGroup
// @Failure 500 {object} ginext.Response
// @Router /rating-group/get-list [get]
func (h *RatingGroupHandlers) List(r *ginext.Request) (*ginext.Response, error) {
	req := model.RatingGroupListRequest{}

	r.MustBind(&req)
	filter := &model.Filter{
		Request: req,
		Pager:   ginext.NewPagerWithGinCtx(r.GinCtx),
	}

	result, err := h.ratingGroupService.FilterRatingGroup(r.Context(), filter)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}
	r.MustNoError(err)

	return ginext.NewResponseWithPager(http.StatusOK, result.Records, result.Filter.Pager), nil
}

// GetOne
// @Tags RatingGroup
// @Title RatingGroup GetOne
// @Description get item by id// @Param	x-user-id	header	string	true	"user id call request"
// @Param	Authorization	header	string	false	"authorization"
// @Param	id  path 	string	true "id item"
// @Success 200 {object} model.RatingGroup
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating-group/get-one/{id} [get]
func (h *RatingGroupHandlers) GetOne(r *ginext.Request) (*ginext.Response, error) {
	ID := &uuid.UUID{}
	if ID = utils.ParseIDFromUri(r.GinCtx); ID == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	ob, err := h.ratingGroupService.GetOneRatingGroup(r.Context(), *ID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_NOT_FOUND, err.Error())
	}
	r.MustNoError(err)

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// Update
// @Tags RatingGroup
// @Title RatingGroup Update
// @Description update item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Param   rating_group body model.RatingGroupRequest true "New RatingGroup"
// @Success 200 {object} ginext.Response //run "swag init --parseDependency --parseInternal -g pkg/service/service.go"
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating-group/update/{id} [put]
func (h *RatingGroupHandlers) Update(r *ginext.Request) (*ginext.Response, error) {
	l := logger.WithCtx(r.Context(), "RatingGroupHandlers.Update")
	req := model.RatingGroupRequest{}
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	if req.ID = utils.ParseIDFromUri(r.GinCtx); req.ID == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	if err := r.GinCtx.BindJSON(&req); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	ob, err := h.ratingGroupService.GetOneRatingGroup(r.Context(), *req.ID)
	if err != nil {
		l.WithError(err).WithField("todo.id", req.ID).Error("failed to query item")
		return nil, ginext.NewError(lib.CODE_NOT_FOUND, err.Error())
	}

	ob.UpdaterID = &currentUser
	lib.Sync(req, ob)

	if err = h.ratingGroupService.UpdateRatingGroup(r.Context(), ob); err != nil {
		return nil, ginext.NewError(lib.CODE_SERVER_ERROR, err.Error())
	}

	return ginext.NewResponseData(http.StatusOK, ob), nil
}

// Delete
// @Tags RatingGroup
// @Title RatingGroup Delete
// @Description delete item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Success 200 {object} ginext.Response
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating-group/delete/{id} [delete]
func (h *RatingGroupHandlers) Delete(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	id := utils.ParseIDFromUri(r.GinCtx)
	if id == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	req := model.DeleteRequest{
		ID:          []uuid.UUID{*id},
		Action:      lib.IS_DELETE_TRASH,
		CurrentUser: &currentUser,
	}
	r.MustNoError(h.ratingGroupService.DeleteRatingGroup(r.Context(), req))

	return ginext.NewResponse(http.StatusOK), nil
}

// Restore
// @Tags RatingGroup
// @Title RatingGroup Restore
// @Description delete item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Success 200 {object} ginext.Response
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating-group/delete/{id} [delete]
func (h *RatingGroupHandlers) Restore(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	id := utils.ParseIDFromUri(r.GinCtx)
	if id == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	req := model.DeleteRequest{
		ID:          []uuid.UUID{*id},
		Action:      lib.REVERT,
		CurrentUser: &currentUser,
	}
	r.MustNoError(h.ratingGroupService.DeleteRatingGroup(r.Context(), req))

	return ginext.NewResponse(http.StatusOK), nil
}

// DeleteHard
// @Tags RatingGroup
// @Title RatingGroup DeleteHard
// @Description delete item by id
// @Param	Authorization	header	string	false	"authorization"
// @Param	x-user-id	header	string	true	"user id call request"
// @Param	id		path 	string	true "id item"
// @Success 200 {object} ginext.Response
// @Success 201 {object} ginext.Response
// @Failure 400 {object} ginext.Response
// @Failure 401 {object} ginext.Response
// @Failure 403 {object} ginext.Response
// @Failure 404 {object} ginext.Response
// @Failure 500 {object} ginext.Response
// @Router /rating-group/delete-hard/{id} [delete]
func (h *RatingGroupHandlers) DeleteHard(r *ginext.Request) (*ginext.Response, error) {
	strUserID := ginext.GetUserID(r.GinCtx)
	currentUser, err := uuid.Parse(strUserID)
	if err != nil {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, err.Error())
	}

	id := utils.ParseIDFromUri(r.GinCtx)
	if id == nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, "Wrong ID")
	}

	req := model.DeleteRequest{
		ID:          []uuid.UUID{*id},
		Action:      lib.IS_DELETE_HARD_TRASH,
		CurrentUser: &currentUser,
	}
	r.MustNoError(h.ratingGroupService.DeleteRatingGroup(r.Context(), req))

	return ginext.NewResponse(http.StatusOK), nil
}
