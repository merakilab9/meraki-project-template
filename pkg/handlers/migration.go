package handlers

import (
	"github.com/go-gormigrate/gormigrate/v2"
	"meraki-project-template/pkg/model"

	"github.com/gin-gonic/gin"
	"gitlab.com/merakilab9/common/logger"
	"gorm.io/gorm"
)

type MigrationHandler struct {
	db *gorm.DB
}

func NewMigrationHandler(db *gorm.DB) *MigrationHandler {
	return &MigrationHandler{db: db}
}

// @Tags Migrate
// @Router  /internal/migrate [post]
func (h *MigrationHandler) Migrate(ctx *gin.Context) {

	log := logger.WithCtx(ctx, "BaseMigrate")
	migrate := gormigrate.New(h.db, gormigrate.DefaultOptions, []*gormigrate.Migration{
		// RUN: cmd/genid to get unique ID
		// WARNING: ADD MIGRATE TO THE END OF THE LIST
		{
			ID: "testt22t",
			Migrate: func(tx *gorm.DB) error {
				return tx.AutoMigrate(&model.ReasonDetail{})
			},
		},
		{
			ID: "20230418110541",
			Migrate: func(tx *gorm.DB) error {
				_ = tx.Exec(`
				CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
				DROP type IF EXISTS rule_default_type; CREATE TYPE rule_default_type AS ENUM ('comment','rating','all');
				DROP type IF EXISTS comment_status; CREATE TYPE comment_status AS ENUM ('approved','rejected','waiting_approve','spam');
				DROP type IF EXISTS entity_relationship_type; CREATE TYPE entity_relationship_type AS ENUM ('primary','reference');
				`)

				models := []interface{}{
					&model.RuleDefault{},
					&model.Comment{},
					&model.CommentTaxonomy{},
					&model.CommentAdjust{},
					&model.CommentTransaction{},
					&model.EntityRelationship{},
					&model.EntityTermTaxonomy{},
					&model.Rating{},
					&model.RatingGroup{},
					&model.RatingDetail{},
					&model.RatingTransaction{},
					&model.RatingAdjust{},
					&model.Reason{},
					&model.ReasonDetail{},
					&model.User{},
				}
				for _, m := range models {
					if err := h.db.AutoMigrate(m); err != nil {
						return err
					}
				}
				return nil
			},
		},
		{
			ID: "20230517165021",
			Migrate: func(tx *gorm.DB) error {
				return tx.Exec(`ALTER TABLE entity_relationship ADD CONSTRAINT uc_object_type_term_ UNIQUE (object_id,object_type,entity_term_taxonomy_id);`).Error
			},
		},
	})
	err := migrate.Migrate()
	if err != nil {
		log.Errorf(err.Error())
	}
}
