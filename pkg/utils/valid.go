package utils

import (
	"github.com/google/uuid"
	"github.com/jinzhu/gorm/dialects/postgres"
)

func String(in *string) string {
	if in == nil {
		return ""
	}
	return *in
}

func Float64(in *float64) float64 {
	if in == nil {
		return 0
	}
	return *in
}

func StringJsonb(in *postgres.Jsonb) postgres.Jsonb {
	var jsonb postgres.Jsonb
	if in == nil {
		return jsonb
	}
	return *in
}

func UUID(req *uuid.UUID) uuid.UUID {
	if req == nil {
		return uuid.Nil
	}
	return *req
}

func ToPointer[T any](in T) *T {
	return &in
}
