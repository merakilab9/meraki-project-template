package utils

const (
	HeaderXRequestID = "x-request-id"
	HeaderUserID     = "x-user-id"
	HeaderUserMeta   = "x-user-type"
)

const (
	IS_DELETE_TRASH      = "trash"
	IS_DELETE_HARD_TRASH = "hard_trash"
	REVERT               = "revert"

	SQL_DELETED_AT      = "deleted_at"
	SQL_HARD_DELETED_AT = "hard_deleted_at"
	SQL_UPDATER_ID      = "updater_id"
)

const (
	ObjectRating      = "rating"
	ObjectComment     = "comment"
	ObjectRuleDefault = "rule_default"
	ObjectReason      = "reason"
	ObjectUser        = "user"
)
