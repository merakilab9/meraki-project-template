package utils

import (
	"encoding/json"

	"github.com/samber/lo"
)

func PickStructJsonKeys[T interface{}](data *T, keys ...string) *T {
	dataMap := StructToMap(data)
	if dataMap == nil {
		return data
	}

	newDataMap := lo.PickByKeys(dataMap, keys)
	return MapToStruct[T](newDataMap)
}

func OmitStructJsonKeys[T interface{}](data *T, keys ...string) *T {
	dataMap := StructToMap(data)
	if dataMap == nil {
		return data
	}

	newDataMap := lo.OmitByKeys(dataMap, keys)
	return MapToStruct[T](newDataMap)
}

func StructToMap[T interface{}](data *T) map[string]interface{} {
	jsonBytes, err := json.Marshal(data)
	if err != nil {
		return nil
	}

	var dataMap map[string]interface{}
	json.Unmarshal(jsonBytes, &dataMap)

	return dataMap
}

func MapToStruct[T interface{}](data map[string]interface{}) *T {
	jsonBytes, err := json.Marshal(data)
	if err != nil {
		return nil
	}

	var dataStruct T
	json.Unmarshal(jsonBytes, &dataStruct)

	return &dataStruct
}
