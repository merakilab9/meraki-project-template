package utils

import "errors"

var (
	ErrNotFound     = errors.New("resource not found")
	ErrConflict     = errors.New("resource already exists")
	ErrBadRequest   = errors.New("bad request")
	ErrForbidden    = errors.New("Forbidden")
	ErrUnauthorized = errors.New("Unauthorized")
)
