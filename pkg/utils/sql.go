package utils

import (
	"fmt"
	"gorm.io/gorm"
	"strings"
)

func MustExist(tx *gorm.DB, model interface{}, field string, id interface{}) bool {
	var count int64 = 0
	err := tx.Model(model).Where(field+" = ?", id).Count(&count).Error
	if err != nil {
		return false
	}
	return count == 0
}

func FilterIfNotNil[T any](in *T, current map[string]interface{}, query string) map[string]interface{} {
	if in != nil {
		current[query] = *in
	}
	return current
}

func FilterILike(in *string, current map[string]interface{}, query string) map[string]interface{} {
	if in != nil {
		current[query] = "%" + *in + "%"
	}
	return current
}

func BuildQuery[T any](params map[string]T, separator string) (string, []T) {
	var condition []string
	var args []T
	for key, values := range params {
		condition = append(condition, key)
		args = append(args, values)
	}
	return strings.Join(condition, fmt.Sprintf(" %s ", separator)), args
}
