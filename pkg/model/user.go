package model

import (
	"time"
)

type User struct {
	BaseModel
	Username        string    `json:"username" gorm:"column:username;type:varchar(255)"`
	LastLogin       time.Time `json:"last_login"`
	ObjectSystemKey string    `json:"object_system_key" gorm:"column:object_system_key;type:varchar(255)"`
	Salt            string    `json:"salt"`
	Guid            string    `json:"guid"`
	IsActive        bool      `json:"is_active" gorm:"column:is_active;not null"`
	Password        string    `json:"password"`
}

func (User) TableName() string {
	return "user"
}
