package model

import "github.com/google/uuid"

type EntityRelationshipType string

const (
	RelationshipTypePrimary   = "primary"
	RelationshipTypeReference = "reference"
)

type EntityRelationship struct {
	BaseModel
	ObjectId               uuid.UUID              `json:"object_id" gorm:"column:object_id;type:uuid;not null"`
	ObjectType             string                 `json:"object_type" gorm:"column:object_type;type:varchar(255)"`
	Order                  int                    `json:"order" gorm:"column:order"`
	EntityRelationshipType EntityRelationshipType `json:"entity_relationship_type" gorm:"type:entity_relationship_type;not null"`
	EntityTermTaxonomyId   uuid.UUID              `json:"entity_term_taxonomy_id" gorm:"column:entity_term_taxonomy_id;type:uuid;not null"`
	EntityTermTaxonomy     EntityTermTaxonomy     `json:"-" gorm:"foreignKey:entity_term_taxonomy_id;references:id"`
}

type EntityRelationshipRequest struct {
	ID                   *uuid.UUID `json:"id,omitempty"`
	ObjectId             uuid.UUID  `json:"object_id" gorm:"column:object_id;type:uuid;not null"`
	ObjectType           string     `json:"object_type" gorm:"column:object_type;type:varchar(255)"`
	EntityTermTaxonomyId uuid.UUID  `json:"entity_term_taxonomy_id" gorm:"column:entity_term_taxonomy_id;type:uuid;not null"`
	Order                int        `json:"order" gorm:"column:order"`
}

func (EntityRelationship) TableName() string {
	return "entity_relationship"
}

type EntityTermTaxonomy struct {
	BaseModel
	EntityTermId uuid.UUID `json:"entity_term_id" gorm:"column:entity_term_id;type:uuid"`
	Taxonomy     string    `json:"taxonomy" gorm:"column:taxonomy;type:varchar(255)"`
	Parent       uuid.UUID `json:"parent" gorm:"column:parent;type:uuid"`
	Count        int       `json:"count" gorm:"column:count"`
	Description  string    `json:"description" gorm:"column:description"`
}

type EntityTermTaxonomyRequest struct {
	ID           *uuid.UUID `json:"id,omitempty"`
	EntityTermId *uuid.UUID `json:"entity_term_id" gorm:"column:entity_term_id;type:uuid"`
	Taxonomy     *string    `json:"taxonomy" gorm:"column:taxonomy;type:varchar(255)"`
	Parent       *uuid.UUID `json:"parent" gorm:"column:parent;type:uuid"`
	Count        *int       `json:"count" gorm:"column:count"`
	Level        *int       `json:"level" gorm:"column:level"`
	Description  *string    `json:"description" gorm:"column:description"`
}

func (EntityTermTaxonomy) TableName() string {
	return "entity_term_taxonomy"
}

type FilterEntityTermTaxonomyResult struct {
	Filter  *Filter
	Records []*EntityTermTaxonomy
}

type FilterEntityRelationshipResult struct {
	Filter  *Filter
	Records []*EntityRelationship
}

type EntityRelationshipByObjectRequest struct {
	ID                        *uuid.UUID  `json:"id,omitempty"`
	ObjectId                  *uuid.UUID  `json:"object_id" valid:"Required"`
	ObjectType                *string     `json:"object_type" valid:"Required"`
	Order                     *int        `json:"order" `
	EntityTermTaxonomyIDs     []uuid.UUID `json:"entity_term_taxonomy_ids" valid:"Required"`
	EntityTermTaxonomyPrimary *uuid.UUID  `json:"entity_term_taxonomy_primary" valid:"Required"`
	CurrentUser               *uuid.UUID  `json:"current_user" valid:"Required"`
}
