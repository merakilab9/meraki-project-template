package model

import (
	"github.com/google/uuid"
	"github.com/lib/pq"
)

type Rating struct {
	BaseModel
	Name              string              `json:"name" gorm:"not null"`
	Key               string              `json:"key" gorm:"unique; not null"`
	IsActive          bool                `json:"is_active"  gorm:"not null; default:true"`
	RatingTransaction []RatingTransaction `json:"rating_transaction,omitempty"`
	RatingGroupId     *uuid.UUID          `json:"rating_group_id" gorm:"not null"`
	Image             *pq.StringArray     `json:"image" gorm:"type:varchar(255)[];" swaggertype:"array,string"`
	RatingGroup       RatingGroup         `json:"rating_group" gorm:"foreignKey:rating_group_id;references:id"`
}

func (Rating) TableName() string {
	return "rating"
}

type RatingTransaction struct {
	BaseModel
	RatingAdjust *RatingAdjust `json:"rating_adjust,omitempty"`
	RatingID     uuid.UUID     `json:"rating_id" gorm:"column:rating_id;type:uuid"`
	Rating       Rating        `json:"-" gorm:"foreignKey:rating_id;references:id"`
}

func (RatingTransaction) TableName() string {
	return "rating_transaction"
}

type RatingAdjust struct {
	BaseModel
	RatingTransactionID uuid.UUID         `json:"rating_transaction_id"  gorm:"column:rating_transaction_id;type:uuid"`
	RatingTransaction   RatingTransaction `json:"-" gorm:"foreignKey:rating_transaction_id;references:id"`
	MinValue            float64           `json:"min_value" gorm:"not null"`
	MaxValue            float64           `json:"max_value" gorm:"not null"`
}

type RatingAdjustRequest struct {
	MinValue *float64 `json:"min_value" `
	MaxValue *float64 `json:"max_value" `
}

func (RatingAdjust) TableName() string {
	return "rating_adjust"
}

type RatingGroup struct {
	BaseModel
	Name        string `json:"name"`
	Key         string `json:"key" gorm:"unique"`
	IsActive    bool   `json:"is_active" gorm:"default:true"`
	Description string `json:"description" gorm:"column:description"`
}

func (RatingGroup) TableName() string {
	return "rating_group"
}

type RatingDetail struct {
	BaseModel
	ObjectId            uuid.UUID         `json:"object_id" gorm:"column:object_id;type:uuid;not null"`
	ObjectType          string            `json:"object_type" gorm:"column:object_type;type:varchar(255)"`
	CountingValue       float64           `json:"counting_value"`
	RatingTransactionID *uuid.UUID        `json:"rating_transaction_id" gorm:"not null"`
	RatingTransaction   RatingTransaction `json:"-" gorm:"foreignKey:rating_transaction_id;references:id"`
}

func (RatingDetail) TableName() string {
	return "rating_detail"
}

type RatingGroupRequest struct {
	ID          *uuid.UUID `json:"id,omitempty"`
	Name        *string    `json:"name" valid:"Required"`
	Key         *string    `json:"key" valid:"Required"`
	IsActive    *bool      `json:"is_active"`
	Description *string    `json:"description" gorm:"column:description"`
}

type RatingRequest struct {
	ID                        *uuid.UUID      `json:"id,omitempty"`
	Name                      *string         `json:"name" form:"name" valid:"Required"`
	Key                       *string         `json:"key" form:"name" valid:"Required"`
	IsActive                  *bool           `json:"is_active" `
	Image                     *pq.StringArray `json:"image"`
	MinValue                  *float64        `json:"min_value" valid:"Required"`
	MaxValue                  *float64        `json:"max_value" valid:"Required"`
	RatingGroupId             *uuid.UUID      `json:"rating_group_id"  valid:"Required"`
	EntityTermTaxonomyIDs     []uuid.UUID     `json:"entity_term_taxonomy_ids" valid:"Required"`
	EntityTermTaxonomyPrimary *uuid.UUID      `json:"entity_term_taxonomy_primary"`
}

type RatingUpdateRequest struct {
	Name          *string          `json:"name" `
	Key           *string          `json:"key" `
	IsActive      *bool            `json:"is_active" `
	RatingGroupId **uuid.UUID      `json:"rating_group_id"`
	Image         **pq.StringArray `json:"image" `
}

type UpdateEntityRequest struct {
	ID                        *uuid.UUID  `json:"id,omitempty"`
	EntityTermTaxonomyIDs     []uuid.UUID `json:"entity_term_taxonomy_ids" valid:"Required"`
	EntityTermTaxonomyPrimary *uuid.UUID  `json:"entity_term_taxonomy_primary"`
}
type RatingGetListRequest struct {
	Name                  *string    `json:"name" form:"name"`
	Key                   *string    `json:"key" form:"name"`
	MinValue              *float64   `json:"min_value"`
	MaxValue              *float64   `json:"max_value"`
	RatingGroupIds        *[]string  `json:"rating_group_ids" form:"rating_group_ids" `
	EntityTermTaxonomyIDs *[]string  `json:"entity_term_taxonomy_ids" form:"entity_term_taxonomy_ids" valid:"Required"`
	CurrentUser           *uuid.UUID `json:"current_user"`
}

type RatingTransactionRequest struct {
	ID                        *uuid.UUID   `json:"id,omitempty"`
	MinValue                  *float64     `json:"min_value"`
	MaxValue                  *float64     `json:"max_value"`
	Image                     []string     `json:"image"`
	EntityTermTaxonomyIDs     *[]uuid.UUID `json:"entity_term_taxonomy_ids"`
	EntityTermTaxonomyPrimary *uuid.UUID   `json:"entity_term_taxonomy_primary"`
}

type RatingDetailRequest struct {
	ID                  *uuid.UUID `json:"id,omitempty"`
	ObjectId            *uuid.UUID `json:"object_id" valid:"Required"`
	ObjectType          *string    `json:"object_type" valid:"Required"`
	CountingValue       *float64   `json:"counting_value" valid:"Required"`
	RatingID            *uuid.UUID `json:"rating_id" valid:"Required"`
	RatingTransactionID *uuid.UUID `json:"rating_transaction_id" valid:"Required"`
}

type UpdateRatingDetailRequest struct {
	ID            *uuid.UUID `json:"id,omitempty"`
	CountingValue *float64   `json:"counting_value" `
}

type FilterRatingGroupResult struct {
	Filter  *Filter
	Records []*RatingGroup
}

type FilterRatingResult struct {
	Filter  *Filter
	Records []*Rating
}

type FilterRatingPublishedResult struct {
	Filter  *Filter
	Records []RatingPublishedResponse
}

type RatingResponse struct {
	Rating
	RatingTransaction []RatingTransactionResponse `json:"rating_transaction" gorm:"foreignkey:rating_id;association_foreignkey:id"`
}

type RatingPublishedResponse struct {
	BaseModel
	Name                string          `json:"name" `
	Key                 string          `json:"key"`
	IsActive            bool            `json:"is_active" `
	RatingGroupId       *uuid.UUID      `json:"rating_group_id"`
	Image               *pq.StringArray `json:"image" `
	RatingGroup         RatingGroup     `json:"rating_group" `
	RatingTransactionID uuid.UUID       `json:"rating_transaction_id" `
	MinValue            float64         `json:"min_value"`
	MaxValue            float64         `json:"max_value" `
}

type RatingTransactionResponse struct {
	RatingTransaction
	RatingAdjust RatingAdjust `json:"rating_adjust" gorm:"foreignkey:rating_transaction_id;association_foreignkey:id"`
}

type FilterRatingDetailResult struct {
	Filter  *Filter
	Records []*RatingDetail
}

type RatingGroupListRequest struct {
	CreatorID *string `json:"creator_id" form:"creator_id"`
	Name      *string `json:"name" form:"name"`
	Key       *string `json:"key"  form:"key"`
	IsActive  *bool   `json:"is_active"  form:"is_active"`
}

type RatingDetailListRequest struct {
	CreatorID           *string    `json:"creator_id" form:"creator_id"`
	ObjectId            *string    `json:"object_id" form:"object_id"`
	ObjectType          *string    `json:"object_type" form:"object_type"`
	CountingValue       *float64   `json:"counting_value" form:"counting_value"`
	RatingTransactionID *uuid.UUID `json:"rating_transaction_id" form:"rating_transaction_id"`
}
