package model

import (
	"github.com/google/uuid"
	"github.com/jinzhu/gorm/dialects/postgres"
)

// TODO: reason entities

type Reason struct {
	BaseModel
	Name        string         `json:"name" gorm:"column:name;type:varchar(255)"`
	Key         string         `json:"key" gorm:"unique;column:key;type:varchar(255)"`
	Description string         `json:"description" gorm:"column:description"`
	Content     postgres.Jsonb `json:"content" gorm:"type:jsonb" swaggertype:"object"`
	IsActive    bool           `json:"is_active" gorm:"column:is_active;not null;default: true"`
}

func (Reason) TableName() string {
	return "reason"
}

type ReasonRequest struct {
	ID                        *uuid.UUID      `json:"id,omitempty"`
	Name                      *string         `json:"name" valid:"Required"`
	Key                       *string         `json:"key" valid:"Required"`
	Description               *string         `json:"description"`
	Content                   *postgres.Jsonb `json:"content"`
	IsActive                  *bool           `json:"is_active"`
	EntityTermTaxonomyIDs     []uuid.UUID     `json:"entity_term_taxonomy_ids" valid:"Required"`
	EntityTermTaxonomyPrimary *uuid.UUID      `json:"entity_term_taxonomy_primary"`
}

type ReasonDetail struct {
	BaseModel
	CommentTransactionID uuid.UUID          `json:"comment_transaction_id"`
	CommentTransaction   CommentTransaction `json:"comment_transaction" gorm:"foreignKey:comment_transaction_id;references:id"`
	ReasonId             uuid.UUID          `json:"reason_id"`
	Reason               Reason             `json:"reason" gorm:"foreignKey:reason_id;references:id"`
}

func (ReasonDetail) TableName() string {
	return "reason_detail"
}

type FilterReasonResult struct {
	Filter  *Filter
	Records []*Reason
}

type FilterReasonDetailResult struct {
	Filter  *Filter
	Records []*ReasonDetail
}

type ReasonListRequest struct {
	ID                    *uuid.UUID `json:"id,omitempty" form:"id"`
	Name                  *string    `json:"name" form:"name"`
	Key                   *string    `json:"key" form:"key"`
	IsActive              *bool      `json:"is_active" form:"is_active"`
	RatingGroupIds        *[]string  `json:"rating_group_ids" form:"rating_group_ids" `
	EntityTermTaxonomyIDs *[]string  `json:"entity_term_taxonomy_ids" form:"entity_term_taxonomy_ids" valid:"Required"`
}
