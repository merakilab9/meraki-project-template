package model

import (
	"gorm.io/gorm/clause"
	"time"

	uuid "github.com/google/uuid"
	"gitlab.com/merakilab9/common/ginext"
	"gorm.io/gorm"
)

type Pagination struct {
	Page     int
	PageSize int
}

type UriParse struct {
	ID []string `json:"id" uri:"id"`
}

type UriParseAction struct {
	Action []string `json:"action" uri:"action"`
}

type BaseModel struct {
	ID            uuid.UUID       `gorm:"primary_key;type:uuid;default:uuid_generate_v4()" json:"id"`
	CreatorID     *uuid.UUID      `json:"creator_id,omitempty"`
	UpdaterID     *uuid.UUID      `json:"updater_id,omitempty"`
	CreatedAt     time.Time       `gorm:"column:created_at;default:CURRENT_TIMESTAMP" json:"created_at"`
	UpdatedAt     time.Time       `gorm:"column:updated_at;default:CURRENT_TIMESTAMP" json:"updated_at"`
	DeletedAt     *gorm.DeletedAt `json:"deleted_at,omitempty"`
	HardDeletedAt *time.Time      `json:"hard_deleted_at,omitempty" gorm:"column:hard_deleted_at"`
}

type Filter struct {
	Request interface{}
	Pager   *ginext.Pager
}

type FilterResult[Model interface{}] struct {
	Filter  *Filter
	Records []*Model
}

type DeleteRequest struct {
	ID          []uuid.UUID `json:"id,omitempty" valid:"Required"`
	Action      string      `json:"action" valid:"Required"`
	CurrentUser *uuid.UUID  `json:"current_user"`
}

type ReqObject struct {
	ObjectType string `json:"object_type"`
	ObjectID   string `json:"object_id"`
}

type BaseRepoQueries struct {
	SortableFields []string
	Joins          map[string][]interface{}
	Preloads       map[string][]interface{}
	Condition      string
	Args           []interface{}
	Clauses        []clause.Expression
}
