package model

import (
	"github.com/google/uuid"
	"github.com/jinzhu/gorm/dialects/postgres"
)

// TODO: valid is spam; is hidden

type CommentStatus string

const (
	CommentStatusApproved       CommentStatus = "approved"
	CommentStatusRejected       CommentStatus = "rejected"
	CommentStatusWaitingApprove CommentStatus = "waiting_approve"
	CommentStatusSpam           CommentStatus = "spam"
)

type Comment struct {
	BaseModel
	ObjectId           uuid.UUID            `json:"object_id" gorm:"column:object_id;type:uuid;not null"`
	ObjectType         string               `json:"object_type" gorm:"column:object_type;type:varchar(255)"`
	AuthorID           uuid.UUID            `json:"author_id"  gorm:"not null; column:author_id;type:uuid;not null"`
	AuthorType         string               `json:"author_type" gorm:"not null; column:author_type;type:varchar(255)"`
	IsHidden           bool                 `json:"is_hidden" gorm:"default:true"`
	IsSpam             bool                 `json:"is_spam" gorm:"default:false"`
	CommentTransaction []CommentTransaction `json:"comment_transaction,omitempty"`
}

type CommentTransaction struct {
	BaseModel
	CommentID     uuid.UUID      `json:"comment_id"  gorm:"column:comment_id;type:uuid"`
	Comment       Comment        `json:"-" gorm:"foreignKey:comment_id;references:id"`
	CommentAdjust *CommentAdjust `json:"comment_adjust,omitempty"`
	Status        CommentStatus  `json:"status" gorm:"not null;type:comment_status;default:waiting_approve"`
}

func (CommentTransaction) TableName() string {
	return "comment_transaction"
}

type CommentAdjust struct {
	BaseModel
	CommentTransactionID uuid.UUID          `json:"comment_transaction_id"  gorm:"column:comment_transaction_id;type:uuid"`
	CommentTransaction   CommentTransaction `json:"-" gorm:"foreignKey:comment_transaction_id;references:id"`
	AuthorName           string             `json:"author_name"`
	AuthorEmail          *string            `json:"author_email"`
	AuthorPhone          *string            `json:"author_phone"`
	AuthorIP             *string            `json:"author_ip"`
	Content              *postgres.Jsonb    `json:"content,omitempty" gorm:"type:jsonb" swaggertype:"object"`
}

func (CommentAdjust) TableName() string {
	return "comment_adjust"
}

type CommentRequest struct {
	ID           *uuid.UUID      `json:"id,omitempty" example:"031cd7a5-7822-4130-8e34-73b2eb5d4624"`
	ObjectId     *uuid.UUID      `json:"object_id" valid:"Required" example:"031cd7a5-7822-4130-8e34-73b2eb5d4624"`
	ObjectType   *string         `json:"object_type" valid:"Required" example:"product"`
	Content      *postgres.Jsonb `json:"content" valid:"Required"`
	ParentID     *uuid.UUID      `json:"parent_id" example:"031cd7a5-7822-4130-8e34-73b2eb5d4624"`
	AuthorID     *uuid.UUID      `json:"author_id" valid:"Required"`
	AuthorType   *string         `json:"author_type" valid:"Required"`
	IsSpam       *bool           `json:"is_spam"`
	IsHidden     *bool           `json:"is_hidden"`
	AuthorEmail  *string         `json:"author_email"`
	AuthorPhone  *string         `json:"author_phone"`
	AuthorIP     *string         `json:"author_ip"`
	DisplayLevel int
}

type UpdateCommentRequest struct {
	ID       *uuid.UUID `json:"id,omitempty" example:"031cd7a5-7822-4130-8e34-73b2eb5d4624"`
	IsSpam   *bool      `json:"is_spam"`
	IsHidden *bool      `json:"is_hidden"`
}

type UpdateCommentContentRequest struct {
	ID                   *uuid.UUID      `json:"id,omitempty" example:"031cd7a5-7822-4130-8e34-73b2eb5d4624"`
	Content              *postgres.Jsonb `json:"content" valid:"Required"`
	AuthorID             *uuid.UUID      `json:"author_id" valid:"Required"`
	AuthorType           *string         `json:"author_type" valid:"Required"`
	CommentTransactionID *uuid.UUID      `json:"comment_transaction_id" valid:"Required"`
}

type CommentTransactionRequest struct {
	ID        *uuid.UUID `json:"id,omitempty" example:"031cd7a5-7822-4130-8e34-73b2eb5d4624"`
	Status    *string    `json:"status"`
	ReasonIDs *[]string  `json:"reason_ids"`
}
type Content struct {
	Text string `json:"text"`
}

type CommentListRequest struct {
	ID         *uuid.UUID `json:"id,omitempty" form:"id"`
	ObjectId   *string    `json:"object_id" form:"object_id"`
	ObjectType *string    `json:"object_type" form:"object_type"`
	AuthorID   *uuid.UUID `json:"author_id"`
	AuthorType *string    `json:"author_type"`
	IsHidden   *bool      `json:"is_hidden" form:"is_hidden"`
	IsSpam     *bool      `json:"is_spam"`
	ParentID   *string    `json:"parent_id" form:"parent_id"`
}

type UpdateTransactionCommentRequest struct {
	ID                   *uuid.UUID     `json:"id" `
	CommentTransactionID *uuid.UUID     `json:"comment_transaction_id" valid:"Required"`
	Status               *CommentStatus `json:"status"  valid:"Required"`
	ReasonIDs            *[]string      `json:"reason_ids"`
}

type FilterCommentResult struct {
	Filter  *Filter
	Records []*Comment
}

type FilterCommentPublishedResult struct {
	Filter  *Filter
	Records []CommentPublishedResponse
}

type CommentPublishedResponse struct {
	BaseModel
	AuthorID             uuid.UUID       `json:"author_id"  `
	AuthorType           string          `json:"author_type" `
	IsHidden             bool            `json:"is_hidden"`
	IsSpam               bool            `json:"is_spam"`
	CommentTransactionID *uuid.UUID      `json:"comment_transaction_id,omitempty"`
	AuthorName           string          `json:"author_name"`
	AuthorEmail          *string         `json:"author_email"`
	AuthorPhone          *string         `json:"author_phone"`
	AuthorIP             *string         `json:"author_ip"`
	Content              *postgres.Jsonb `json:"content,omitempty"`
	Status               CommentStatus   `json:"status"`
}

func (Comment) TableName() string {
	return "comment"
}

type CommentTaxonomy struct {
	BaseModel
	CommentID uuid.UUID `json:"comment_id"  gorm:"column:comment_id;type:uuid"`
	Comment   Comment   `json:"comment" gorm:"foreignKey:comment_id;references:id"`
	Taxonomy  string    `json:"taxonomy" gorm:"column:taxonomy;type:varchar(255)"`
	Parent    uuid.UUID `json:"parent" gorm:"column:parent;type:uuid"`
	Level     int       `json:"level" gorm:"column:level"`
}

func (CommentTaxonomy) TableName() string {
	return "comment_taxonomy"
}
