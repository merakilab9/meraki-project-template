package model

import (
	"github.com/google/uuid"
)

type RuleDefaultType string

const (
	AllRuleType     RuleDefaultType = "all"
	CommentRuleType RuleDefaultType = "comment"
	RatingRuleType  RuleDefaultType = "rating"
)

type RuleDefault struct {
	BaseModel
	RoundingDecimal int             `json:"rounding_decimal"`
	MaxDisplayLevel int             `json:"max_display_level"`
	MinimumLength   int             `json:"minimum_length"`
	MaximumLength   int             `json:"maximum_length"`
	Name            string          `json:"name" gorm:"column:name;type:varchar(255)"`
	Key             string          `json:"key" gorm:"column:key;type:varchar(255);unique"`
	Description     string          `json:"description" gorm:"column:description"`
	Type            RuleDefaultType `json:"type" gorm:"not null;type:rule_default_type"`
	LoginRequired   bool            `json:"login_required"`
	IsIncludedLink  bool            `json:"is_included_link"`
	IsActive        bool            `json:"is_active" gorm:"column:is_active;not null;default:false"`
	OriginalID      *uuid.UUID      `json:"original_id"`
}

// TODO: valid login, is includedLink, is_active
// TODO: handle originalID
// TODO: valid comment rules
// TODO: valid rating rules

func (RuleDefault) TableName() string {
	return "rule_default"
}

type RuleRequest struct {
	ID                        *uuid.UUID       `json:"id,omitempty"`
	RoundingDecimal           *int             `json:"rounding_decimal"`
	MaxDisplayLevel           *int             `json:"max_display_level"`
	MinimumLength             *int             `json:"minimum_length"`
	MaximumLength             *int             `json:"maximum_length"`
	Name                      *string          `json:"name" valid:"Required"`
	Key                       *string          `json:"key" valid:"Required"`
	Description               *string          `json:"description" `
	Type                      *RuleDefaultType `json:"type" valid:"Required"`
	LoginRequired             *bool            `json:"login_required"`
	IsIncludedLink            *bool            `json:"is_included_link"`
	IsActive                  *bool            `json:"is_active" `
	EntityTermTaxonomyIDs     []uuid.UUID      `json:"entity_term_taxonomy_ids"`
	EntityTermTaxonomyPrimary *uuid.UUID       `json:"entity_term_taxonomy_primary"`
	OriginalID                **uuid.UUID      `json:"original_id" `
}

type UpdateRuleRequest struct {
	ID              *uuid.UUID       `json:"id,omitempty"`
	RoundingDecimal *int             `json:"rounding_decimal"`
	MaxDisplayLevel *int             `json:"max_display_level"`
	MinimumLength   *int             `json:"minimum_length"`
	MaximumLength   *int             `json:"maximum_length"`
	Name            *string          `json:"name" `
	Key             *string          `json:"key" `
	Description     *string          `json:"description" `
	Type            *RuleDefaultType `json:"type" valid:"Required"`
	LoginRequired   *bool            `json:"login_required"`
	IsIncludedLink  *bool            `json:"is_included_link"`
	IsActive        *bool            `json:"is_active" `
	OriginalID      **uuid.UUID      `json:"original_id" `
}

type RuleCreateRequest struct {
	RoundingDecimal *int    `json:"rounding_decimal"`
	MaxDisplayLevel *int    `json:"max_display_level"`
	MinimumLength   *int    `json:"minimum_length"`
	MaximumLength   *int    `json:"maximum_length"`
	Name            *string `json:"name" gorm:"column:name;type:varchar(255)"`
	Key             *string `json:"key" gorm:"column:key;type:varchar(255)"`
	Description     *string `json:"description" gorm:"column:description"`
	Type            *string `json:"type" gorm:"not null;type:rule_default_type"`
	LoginRequired   *bool   `json:"login_required"`
	IsIncludedLink  *bool   `json:"is_included_link"`
	IsActive        *bool   `json:"is_active" gorm:"column:is_active;not null"`
}

type RuleListRequest struct {
	CreatorID             *string          `json:"creator_id" form:"creator_id"`
	Name                  *string          `json:"name" form:"name"`
	Key                   *string          `json:"key"  form:"key"`
	Type                  *RuleDefaultType `json:"type"  form:"type"`
	EntityTermTaxonomyIDs *[]string        `json:"entity_term_taxonomy_ids" form:"entity_term_taxonomy_ids" valid:"Required"`
	OriginalID            **uuid.UUID      `json:"original_id"`
}

type FilterRuleResult struct {
	Filter  *Filter
	Records []*RuleDefault
}

type ValidRuleRequest struct {
	EntityTermID uuid.UUID   `json:"entity_term_id"`
	Type         string      `json:"type"`
	Data         interface{} `json:"data"`
}

type RuleDefaultByEntities struct {
	RuleDefault
	EntityRelationshipType EntityRelationshipType `json:"entity_relationship_type" gorm:"type:entity_relationship_type;not null"`
	EntityTermTaxonomyId   uuid.UUID              `json:"entity_term_taxonomy_id" gorm:"column:entity_term_taxonomy_id;type:uuid;not null"`
	EntityTermTaxonomy     EntityTermTaxonomy     `json:"-" gorm:"foreignKey:entity_term_taxonomy_id;references:id"`
}
