package model

import (
	"github.com/google/uuid"
	"time"
)

type PublishMock struct {
	BaseModel
	ObjectId   uuid.UUID `json:"object_id" gorm:"column:object_id;type:uuid;not null"`
	ObjectType string    `json:"object_type" gorm:"column:object_type;type:varchar(255)"`
	StartTime  time.Time `json:"start_time" gorm:"not null"`
	EndTime    time.Time `json:"end_time" gorm:"not null"`
}

type PublishMockRequest struct {
	ObjectId   *uuid.UUID `json:"object_id" `
	ObjectType *string    `json:"object_type" `
	StartTime  *time.Time `json:"start_time" `
	EndTime    *time.Time `json:"end_time" `
	AtTime     *time.Time `json:"at_time"`
}
