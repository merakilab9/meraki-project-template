package service

import (
	"meraki-project-template/pkg/model"
)

type PublishingService struct {
}

func NewPublishingService() PublishingInterface {
	return &PublishingService{}
}

type PublishingInterface interface {
	CurrentPublishingRatings(in []model.RatingTransaction) *model.RatingTransaction
	CurrentPublishingComments(in []model.CommentTransaction) *model.CommentTransaction
}

func (s *PublishingService) CurrentPublishingRatings(in []model.RatingTransaction) *model.RatingTransaction {
	if len(in) > 0 {
		return &in[0]
	}
	return nil
}
func (s *PublishingService) CurrentPublishingComments(in []model.CommentTransaction) *model.CommentTransaction {
	if len(in) > 0 {
		return &in[0]
	}
	return nil
}
