package service

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	"github.com/jinzhu/gorm/dialects/postgres"
	"github.com/sirupsen/logrus"
	"gitlab.com/merakilab9/common/ginext"
	"gitlab.com/merakilab9/common/lib"
	"gorm.io/gorm"
	"meraki-project-template/pkg/model"
	"meraki-project-template/pkg/repo"
	"meraki-project-template/pkg/utils"
	"strings"
)

type RuleDefaultService struct {
	baseRepo                   repo.RepositoryInterface[model.RuleDefault]
	baseRepoEntityRelationship repo.RepositoryInterface[model.EntityRelationship]
	baseRepoEntityTermTaxonomy repo.RepositoryInterface[model.EntityTermTaxonomy]
	relationshipService        EntityRelationshipInterface
}

func NewRuleDefaultService(
	baseRepo repo.RepositoryInterface[model.RuleDefault],
	baseRepoEntityRelationship repo.RepositoryInterface[model.EntityRelationship],
	baseRepoEntityTermTaxonomy repo.RepositoryInterface[model.EntityTermTaxonomy],
	relationshipService EntityRelationshipInterface) RuleDefaultInterface {
	return &RuleDefaultService{
		baseRepo:                   baseRepo,
		baseRepoEntityRelationship: baseRepoEntityRelationship,
		baseRepoEntityTermTaxonomy: baseRepoEntityTermTaxonomy,
		relationshipService:        relationshipService,
	}
}

type RuleDefaultInterface interface {
	CreateRuleDefault(ctx context.Context, req *model.RuleRequest, owner *uuid.UUID) (*model.RuleDefault, error)
	FilterRuleDefault(ctx context.Context, filter *model.Filter) (*model.FilterRuleResult, error)
	GetOneRuleDefault(ctx context.Context, id uuid.UUID) (*model.RuleDefault, error)
	UpdateRuleDefault(ctx context.Context, req *model.UpdateRuleRequest, currentUser uuid.UUID, entityTaxonomyID uuid.UUID) (*model.RuleDefault, error)
	UpdateRuleEntity(ctx context.Context, req *model.UpdateEntityRequest, currentUser uuid.UUID, entityTaxonomyID uuid.UUID) (*model.RuleDefault, error)
	DeleteRuleDefault(ctx context.Context, req model.DeleteRequest, entityTaxonomyID uuid.UUID) error
	ValidRule(ctx context.Context, req model.ValidRuleRequest) error
}

func (s *RuleDefaultService) CreateRuleDefault(ctx context.Context, req *model.RuleRequest, owner *uuid.UUID) (*model.RuleDefault, error) {
	var err error
	tx := s.baseRepo.RepoTxn().Begin()
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	ob := &model.RuleDefault{
		BaseModel: model.BaseModel{
			CreatorID: owner,
		},
	}

	lib.Sync(*req, ob)

	if req.EntityTermTaxonomyPrimary == nil {
		req.EntityTermTaxonomyPrimary = s.relationshipService.GetPrimaryEntityFromEntities(ctx, req.EntityTermTaxonomyIDs)
	}

	err = s.ValidCurrentActiveRule(tx, ctx, req)
	if err != nil {
		return nil, err
	}

	err = s.baseRepo.Create(tx, ctx, ob)
	if err != nil {
		return nil, err
	}

	err = s.relationshipService.CreateEntityRelationshipByObject(tx, ctx, &model.EntityRelationshipByObjectRequest{
		ObjectId:                  &ob.ID,
		ObjectType:                utils.ToPointer("rule_default"),
		EntityTermTaxonomyIDs:     req.EntityTermTaxonomyIDs,
		EntityTermTaxonomyPrimary: req.EntityTermTaxonomyPrimary,
		CurrentUser:               owner,
	})
	if err != nil {
		return nil, err
	}

	rs, err := s.baseRepo.FindOne(tx, ctx, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{ob.ID},
	})
	if err != nil {
		return nil, err
	}

	return rs, nil
}

func (s *RuleDefaultService) ValidCurrentActiveRule(tx *gorm.DB, ctx context.Context, req *model.RuleRequest) error {
	// if type = rating; all current rating rule must = deactive
	currentPrimaryActiveRuleDefaults, err := s.baseRepo.Find(tx, ctx, model.BaseRepoQueries{
		Joins: map[string][]interface{}{
			"inner join entity_relationship on entity_relationship.object_id = rule_default.id AND entity_relationship.object_type = ?": {"rule_default"}},
		Condition: "entity_relationship.entity_relationship_type = ? AND rule_default.is_active = ? AND entity_relationship.entity_term_taxonomy_id = ?",
		Args:      []interface{}{model.RelationshipTypePrimary, true, req.EntityTermTaxonomyPrimary},
	})
	if err != nil {
		return err
	}

	// if type = all; all current rule must = deactive
	if *req.Type == model.AllRuleType && len(currentPrimaryActiveRuleDefaults) > 0 {
		return fmt.Errorf("cannot create rule type <%s> for entity because there is an active rule", strings.ToUpper(string(*req.Type)))
	}
	// if type = comment; current all AND comment rule must = deactive
	// if type = rating; current all AND rating rule must = deactive
	if *req.Type == model.CommentRuleType || *req.Type == model.RatingRuleType {
		for _, ruleDefault := range currentPrimaryActiveRuleDefaults {
			if ruleDefault.Type == model.AllRuleType || ruleDefault.Type == *req.Type {
				return fmt.Errorf("cannot create rule type <%s> for entity because there is an active rule: %s", strings.ToUpper(string(*req.Type)), ruleDefault.Name)
			}
		}
	}
	return nil
}

func (s *RuleDefaultService) UpdateRuleDefault(ctx context.Context, req *model.UpdateRuleRequest, currentUser uuid.UUID, entityTaxonomyID uuid.UUID) (*model.RuleDefault, error) {
	var err error
	tx := s.baseRepo.RepoTxn().Begin()
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	rule, err := s.baseRepo.FindOne(tx, ctx, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{req.ID.String()},
	})
	if err != nil {
		return nil, ginext.NewError(lib.CODE_NOT_FOUND, err.Error())
	}

	// if update type
	if *req.Type != rule.Type {
		if err := s.ValidCurrentActiveRule(nil, ctx, &model.RuleRequest{
			Type:                      req.Type,
			EntityTermTaxonomyPrimary: &entityTaxonomyID,
		}); err != nil {
			return nil, err
		}
	}

	lib.Sync(*req, rule)

	rule.UpdaterID = &currentUser
	if err := s.baseRepo.Update(tx, ctx, rule, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{rule.ID},
	}); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	return rule, nil
}

func (s *RuleDefaultService) UpdateRuleEntity(ctx context.Context, req *model.UpdateEntityRequest, currentUser uuid.UUID, entityTaxonomyID uuid.UUID) (*model.RuleDefault, error) {
	var err error
	tx := s.baseRepo.RepoTxn().Begin()
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	rule, err := s.baseRepo.FindOne(nil, ctx, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{req.ID.String()},
	})
	if err != nil {
		return nil, ginext.NewError(lib.CODE_NOT_FOUND, err.Error())
	}

	// khong check primary user
	//entityRelate, err := s.baseRepoEntityRelationship.
	//	FindOne(tx, ctx, model.BaseRepoQueries{
	//		Condition: "object_id = ? AND object_type = ? AND entity_term_taxonomy_id = ?",
	//		Args:      []interface{}{rule.ID.String(), "rule_default", entityTaxonomyID.String()},
	//	})
	//if err != nil {
	//	return nil, ginext.NewError(lib.CODE_FORBIDDEN, fmt.Sprintf("invalid entity relationship"))
	//}
	//if entityRelate.EntityRelationshipType != model.RelationshipTypePrimary {
	//	return nil, ginext.NewError(lib.CODE_FORBIDDEN, fmt.Sprintf("invalid entity relationship"))
	//}

	// valid new entity current active rule
	if req.EntityTermTaxonomyPrimary == nil {
		req.EntityTermTaxonomyPrimary = s.relationshipService.GetPrimaryEntityFromEntities(ctx, req.EntityTermTaxonomyIDs)
	}

	err = s.ValidCurrentActiveRule(tx, ctx, &model.RuleRequest{
		Type:                      &rule.Type,
		EntityTermTaxonomyPrimary: req.EntityTermTaxonomyPrimary,
	})
	if err != nil {
		return nil, err
	}

	//update entity relationship:
	// Remove tat ca cac entity relation hien tại cua object id
	err = s.relationshipService.RemoveEntityRelationshipByObject(tx, ctx, &model.EntityRelationshipByObjectRequest{
		ObjectId:    &rule.ID,
		ObjectType:  utils.ToPointer("rule_default"),
		CurrentUser: &currentUser,
	})
	if err != nil {
		return nil, err
	}

	// Update đè lên relationship hiện tại
	// get primary if null
	err = s.relationshipService.CreateEntityRelationshipByObject(tx, ctx, &model.EntityRelationshipByObjectRequest{
		ObjectId:                  &rule.ID,
		ObjectType:                utils.ToPointer("rule_default"),
		EntityTermTaxonomyIDs:     req.EntityTermTaxonomyIDs,
		EntityTermTaxonomyPrimary: req.EntityTermTaxonomyPrimary,
		CurrentUser:               &currentUser,
	})
	if err != nil {
		return nil, err
	}

	return rule, nil
}

func (s *RuleDefaultService) DeleteRuleDefault(ctx context.Context, req model.DeleteRequest, entityTaxonomyID uuid.UUID) error {
	//for _, rule := range req.ID {
	//	entityRelate, err := s.baseRepoEntityRelationship.
	//		FindOne(nil, ctx, model.BaseRepoQueries{
	//			Condition: "object_id = ? AND object_type = ? AND entity_term_taxonomy_id = ?",
	//			Args:      []interface{}{rule.String(), "rule_default", entityTaxonomyID.String()},
	//		})
	//	if err != nil {
	//		return ginext.NewError(lib.CODE_FORBIDDEN, fmt.Sprintf("invalid entity relationship"))
	//	}
	//	if entityRelate.EntityRelationshipType != model.RelationshipTypePrimary {
	//		return ginext.NewError(lib.CODE_FORBIDDEN, fmt.Sprintf("invalid entity relationship"))
	//	}
	//}
	return s.baseRepo.DeleteAction(nil, ctx, req)
}

func (s *RuleDefaultService) GetOneRuleDefault(ctx context.Context, id uuid.UUID) (*model.RuleDefault, error) {
	return s.baseRepo.FindOne(nil, ctx, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{id},
	})
}

func (s *RuleDefaultService) FilterRuleDefault(ctx context.Context, filter *model.Filter) (*model.FilterRuleResult, error) {
	req, ok := filter.Request.(model.RuleListRequest)
	if !ok {
		return nil, fmt.Errorf("invalid filter request")
	}
	queryParams := map[string]interface{}{}
	queryParams = utils.FilterILike(req.Name, queryParams, "name ILIKE ?")
	queryParams = utils.FilterIfNotNil(utils.ToPointer("rule_default"), queryParams, "entity_relationship.object_type = ?")
	queryParams = utils.FilterIfNotNil(req.EntityTermTaxonomyIDs, queryParams, "entity_relationship.entity_term_taxonomy_id in (?)")
	condition, args := utils.BuildQuery(queryParams, "AND")

	result, err := s.baseRepo.Filter(nil, ctx, filter, model.BaseRepoQueries{
		SortableFields: []string{"name", "created_at"},
		Joins: map[string][]interface{}{
			"inner join entity_relationship on entity_relationship.object_id = rule_default.id AND entity_relationship.object_type = ?": {"rule_default"},
		},
		Condition: condition,
		Args:      args,
	})
	if err != nil {
		return nil, err
	}

	return &model.FilterRuleResult{
		Filter:  result.Filter,
		Records: result.Records,
	}, nil
}

func (s *RuleDefaultService) ValidRule(ctx context.Context, req model.ValidRuleRequest) error {
	// valid rule
	// case comment
	// case rating
	// case all
	var currentRule *model.RuleDefault
	currentRule, err := s.baseRepo.FindOne(nil, ctx, model.BaseRepoQueries{
		Joins: map[string][]interface{}{
			"LEFT JOIN entity_relationship er on er.object_id = rule_default.id AND er.object_type = ? AND er.entity_relationship_type = ?": {utils.ObjectRuleDefault, model.RelationshipTypePrimary},
		},
		Condition: "rule_default.is_active = ? AND rule_default.type = ? and er.entity_term_taxonomy_id = ?",
		Args:      []interface{}{true, model.AllRuleType, req.EntityTermID.String()},
	})
	if err != nil && !utils.IsErrNotFound(err) {
		return err
	}
	if utils.IsErrNotFound(err) {
		// entity dont have rule type == all
		// get comment or rating rule
		currentRule, err = s.baseRepo.FindOne(nil, ctx, model.BaseRepoQueries{
			Joins: map[string][]interface{}{
				"LEFT JOIN entity_relationship er on er.object_id = rule_default.id AND er.object_type = ? AND er.entity_relationship_type = ?": {utils.ObjectRuleDefault, model.RelationshipTypePrimary},
			},
			Condition: "rule_default.is_active = ? AND rule_default.type = ? and er.entity_term_taxonomy_id = ?",
			Args:      []interface{}{true, req.Type, req.EntityTermID.String()},
		})
		if err != nil && !utils.IsErrNotFound(err) {
			return err
		}
		// if not found any rule, return true, auto pass
		if utils.IsErrNotFound(err) {
			logrus.Info("Entity dont have rule default. Valid rule passed")
			return nil
		}
	}
	switch req.Type {
	case utils.ObjectRating:
		req, ok := req.Data.(*model.RatingDetail)
		if !ok {
			return fmt.Errorf(" rulerating: invalid request valid rule")
		}
		return s.ValidRatingCountingValuePrecise(req.CountingValue, currentRule.RoundingDecimal)
	case utils.ObjectComment:
		req, ok := req.Data.(*model.CommentRequest)
		if !ok {
			return fmt.Errorf("rule comment: invalid request valid rule")
		}
		if err = s.ValidLengthComment(req.Content, currentRule.MinimumLength, currentRule.MaximumLength); err != nil {
			return err
		}
		if err = s.ValidDisplayLevelComment(req.DisplayLevel, currentRule.MaxDisplayLevel); err != nil {
			return err
		}
		if err = s.ValidIncludeLinkComment(req.Content, currentRule.IsIncludedLink); err != nil {
			return err
		}
	default:
		return fmt.Errorf("request valid rule type not found")
	}
	return nil
}

func (s *RuleDefaultService) ValidRatingCountingValuePrecise(req float64, rulePrecise int) error {
	if rulePrecise == 0 {
		// no rulePrecise for counting value
		return nil
	}
	precise := fmt.Sprintf("%g", req)
	tmp := strings.Split(precise, ".")
	if len(tmp) == 1 {
		// int value; case 5 or 5.00; auto pass
		return nil
	}
	if len(tmp) != 2 {
		return fmt.Errorf("rule rating: invalid counting value.")
	}
	// different precision with rating rule
	// ex: rule.RoundingDecimal =  2
	// counting_value must == 231.xx
	if len(tmp[1]) != rulePrecise {
		return fmt.Errorf("rule rating: invalid rounding decimal")
	}
	if len(tmp[1]) == rulePrecise {
		return nil
	}
	return fmt.Errorf("rule rating: invalid rounding decimal")
}
func (s *RuleDefaultService) ValidLengthComment(reqContent *postgres.Jsonb, min, max int) error {
	if reqContent == nil {
		return fmt.Errorf("content not found")
	}
	content := map[string]interface{}{}
	err := json.Unmarshal(reqContent.RawMessage, &content)
	if err != nil {
		return fmt.Errorf("invalid content")
	}

	text, ok := content["text"].(string)
	if ok {
		if min > 0 && len(text) < min {
			return fmt.Errorf("rule comment: below min text length")
		}
		if max > 0 && len(text) > max {
			return fmt.Errorf("rule comment: above max text length")
		}
	}

	return nil
}
func (s *RuleDefaultService) ValidDisplayLevelComment(currentLevel int, max int) error {
	if max > 0 {
		if currentLevel > max {
			return fmt.Errorf("rule comment: reach maxinum display level")
		}
	}
	return nil
}

func (s *RuleDefaultService) ValidIncludeLinkComment(reqContent *postgres.Jsonb, includeLink bool) error {
	if !includeLink {
		content := map[string]interface{}{}
		err := json.Unmarshal(reqContent.RawMessage, &content)
		if err != nil {
			return fmt.Errorf("invalid content")
		}

		text, ok := content["text"].(string)
		if ok {
			if strings.Contains(text, "http://") || strings.Contains(text, "https://") {
				return fmt.Errorf("rule comment: content text cannot include link")
			}
		}
	}
	return nil
}
