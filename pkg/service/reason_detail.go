package service

//
//import (
//	"context"
//	"fmt"
//	"meraki-project-template/pkg/model"
//	"meraki-project-template/pkg/repo"
//	"meraki-project-template/pkg/utils"
//
//	"github.com/google/uuid"
//)
//
//type ReasonDetailService struct {
//	baseRepo repo.RepositoryInterface[model.ReasonDetail]
//}
//
//func NewReasonDetailService(baseRepo repo.RepositoryInterface[model.ReasonDetail]) ReasonDetailInterface {
//	return &ReasonDetailService{baseRepo: baseRepo}
//}
//
//type ReasonDetailInterface interface {
//	CreateReasonDetail(ctx context.Context, ob *model.ReasonDetail) error
//	FilterReasonDetail(ctx context.Context, filter *model.Filter) (*model.FilterReasonDetailResult, error)
//	GetOneReasonDetail(ctx context.Context, id uuid.UUID) (*model.ReasonDetail, error)
//	UpdateReasonDetail(ctx context.Context, ob *model.ReasonDetail) error
//	DeleteReasonDetail(ctx context.Context, req model.DeleteRequest) error
//}
//
//func (s *ReasonDetailService) CreateReasonDetail(ctx context.Context, ob *model.ReasonDetail) error {
//	return s.baseRepo.Create(ctx, ob)
//}
//func (s *ReasonDetailService) UpdateReasonDetail(ctx context.Context, ob *model.ReasonDetail) error {
//	return s.baseRepo.Update(ctx, ob, "id = ?", ob.ID)
//}
//
//func (s *ReasonDetailService) DeleteReasonDetail(ctx context.Context, req model.DeleteRequest) error {
//	return s.baseRepo.DeleteAction(ctx, req)
//}
//
//func (s *ReasonDetailService) GetOneReasonDetail(ctx context.Context, id uuid.UUID) (*model.ReasonDetail, error) {
//	return s.baseRepo.FindOne(ctx, nil, nil, "id = ?", id)
//}
//
//func (s *ReasonDetailService) FilterReasonDetail(ctx context.Context, filter *model.Filter) (*model.FilterReasonDetailResult, error) {
//	req, ok := filter.Request.(model.RuleListRequest)
//	if !ok {
//		return nil, fmt.Errorf("invalid filter request")
//	}
//	queryParams := map[string]interface{}{}
//	queryParams = utils.FilterIfNotNil(req.Name, queryParams, "name ILIKE %?%")
//	queryParams = utils.FilterIfNotNil(req.Type, queryParams, "type = ?")
//	condition, args := utils.BuildQuery(queryParams, "AND")
//
//	result, err := s.baseRepo.Filter(ctx, filter, []string{"name", "created_at"}, nil, nil, condition, args)
//	if err != nil {
//		return nil, err
//	}
//
//	return &model.FilterReasonDetailResult{
//		Filter:  result.Filter,
//		Records: result.Records,
//	}, nil
//}
