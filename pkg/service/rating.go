package service

import (
	"context"
	"fmt"
	"gorm.io/gorm"
	"meraki-project-template/pkg/model"
	"meraki-project-template/pkg/repo"
	"meraki-project-template/pkg/utils"

	"github.com/google/uuid"
	"gitlab.com/merakilab9/common/ginext"
	"gitlab.com/merakilab9/common/lib"
)

type RatingService struct {
	baseRepoRating             repo.RepositoryInterface[model.Rating]
	baseRepoRatingTransaction  repo.RepositoryInterface[model.RatingTransaction]
	baseRepoRatingAdjust       repo.RepositoryInterface[model.RatingAdjust]
	baseRepoEntityRelationship repo.RepositoryInterface[model.EntityRelationship]
	baseRepoEntityTermTaxonomy repo.RepositoryInterface[model.EntityTermTaxonomy]
	baseRepoRuleDefault        repo.RepositoryInterface[model.RuleDefault]
	publishingService          PublishingInterface
	relationshipService        EntityRelationshipInterface
	ruleService                RuleDefaultInterface
}

func NewRatingService(baseRepoRating repo.RepositoryInterface[model.Rating],
	baseRepoRatingTransaction repo.RepositoryInterface[model.RatingTransaction],
	baseRepoRatingAdjust repo.RepositoryInterface[model.RatingAdjust],
	baseRepoEntityRelationship repo.RepositoryInterface[model.EntityRelationship],
	baseRepoEntityTermTaxonomy repo.RepositoryInterface[model.EntityTermTaxonomy],
	baseRepoRuleDefault repo.RepositoryInterface[model.RuleDefault],
	publishingService PublishingInterface,
	relationshipService EntityRelationshipInterface,
	ruleService RuleDefaultInterface,
) RatingInterface {
	return &RatingService{baseRepoRating: baseRepoRating,
		baseRepoRatingTransaction:  baseRepoRatingTransaction,
		baseRepoRatingAdjust:       baseRepoRatingAdjust,
		baseRepoEntityRelationship: baseRepoEntityRelationship,
		baseRepoEntityTermTaxonomy: baseRepoEntityTermTaxonomy,
		baseRepoRuleDefault:        baseRepoRuleDefault,
		publishingService:          publishingService,
		relationshipService:        relationshipService,
		ruleService:                ruleService,
	}
}

type RatingInterface interface {
	CreateRating(ctx context.Context, ob *model.RatingRequest, owner uuid.UUID) (*model.Rating, error)
	GetOneRating(ctx context.Context, id uuid.UUID) (*model.Rating, error)
	UpdateRating(ctx context.Context, req *model.RatingRequest, owner uuid.UUID, entityTaxonomyID uuid.UUID) (*model.Rating, error)
	UpdateRatingEntity(ctx context.Context, req *model.UpdateEntityRequest, currentUser uuid.UUID, entityTaxonomyID uuid.UUID) (*model.Rating, error)
	DeleteRating(ctx context.Context, req model.DeleteRequest, entityTaxonomyID uuid.UUID) error
	GetListRating(ctx context.Context, filter *model.Filter) (*model.FilterRatingResult, error)
	GetListRatingPublished(ctx context.Context, filter *model.Filter) (*model.FilterRatingPublishedResult, error)
}

func (s *RatingService) CreateRating(ctx context.Context, req *model.RatingRequest, owner uuid.UUID) (*model.Rating, error) {
	var err error
	tx := s.baseRepoRating.RepoTxn().Begin()
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	rating := &model.Rating{
		BaseModel: model.BaseModel{
			CreatorID: &owner,
		},
		Name:          utils.String(req.Name),
		Key:           utils.String(req.Key),
		RatingGroupId: req.RatingGroupId,
		Image:         req.Image,
	}

	err = s.baseRepoRating.Create(tx, ctx, rating)
	if err != nil {
		return nil, err
	}

	ratingTransaction := &model.RatingTransaction{
		BaseModel: model.BaseModel{
			CreatorID: &owner,
		},
		RatingID: rating.ID,
	}

	err = s.baseRepoRatingTransaction.Create(tx, ctx, ratingTransaction)
	if err != nil {
		return nil, err
	}

	ratingAdjust := &model.RatingAdjust{
		BaseModel: model.BaseModel{
			CreatorID: &owner,
		},
		RatingTransactionID: ratingTransaction.ID,
		MinValue:            utils.Float64(req.MinValue),
		MaxValue:            utils.Float64(req.MaxValue),
	}

	err = s.baseRepoRatingAdjust.Create(tx, ctx, ratingAdjust)
	if err != nil {
		return nil, err
	}

	// get primary if null
	if req.EntityTermTaxonomyPrimary == nil {
		req.EntityTermTaxonomyPrimary = s.relationshipService.GetPrimaryEntityFromEntities(ctx, req.EntityTermTaxonomyIDs)
	}
	err = s.relationshipService.CreateEntityRelationshipByObject(tx, ctx, &model.EntityRelationshipByObjectRequest{
		ObjectId:                  &rating.ID,
		ObjectType:                utils.ToPointer(utils.ObjectRating),
		EntityTermTaxonomyIDs:     req.EntityTermTaxonomyIDs,
		EntityTermTaxonomyPrimary: req.EntityTermTaxonomyPrimary,
		CurrentUser:               &owner,
	})
	if err != nil {
		return nil, err
	}

	rs, err := s.baseRepoRating.FindOne(tx, ctx, model.BaseRepoQueries{
		Preloads: map[string][]interface{}{
			"RatingGroup": nil,
			"RatingTransaction": {
				func(db *gorm.DB) *gorm.DB {
					db = db.Preload("RatingAdjust").Order("created_at DESC")
					return db
				},
			},
		},
		Condition: "id = ?",
		Args:      []interface{}{rating.ID.String()},
	})
	if err != nil {
		return nil, err
	}

	return rs, nil
}

func (s *RatingService) UpdateRating(ctx context.Context, req *model.RatingRequest, currentUser uuid.UUID, entityTaxonomyID uuid.UUID) (*model.Rating, error) {
	var err error
	tx := s.baseRepoRating.RepoTxn().Begin()
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	rating, err := s.baseRepoRating.FindOne(nil, ctx, model.BaseRepoQueries{
		Preloads: map[string][]interface{}{
			"RatingTransaction": {
				func(db *gorm.DB) *gorm.DB {
					db = db.Preload("RatingAdjust").Order("created_at DESC")
					return db
				},
			},
		},
		Condition: "id = ?",
		Args:      []interface{}{req.ID.String()},
	})
	if err != nil {
		return nil, ginext.NewError(lib.CODE_NOT_FOUND, err.Error())
	}

	// khong check primary user
	//entityRelate, err := s.baseRepoEntityRelationship.
	//	FindOne(nil, ctx, model.BaseRepoQueries{
	//		Condition: "object_id = ? AND object_type = ? AND entity_term_taxonomy_id = ?",
	//		Args:      []interface{}{rating.ID.String(), "rating", entityTaxonomyID.String()},
	//	})
	//if err != nil {
	//	return nil, ginext.NewError(lib.CODE_FORBIDDEN, fmt.Sprintf("invalid entity relationship"))
	//}
	//if entityRelate.EntityRelationshipType != model.RelationshipTypePrimary {
	//	return nil, ginext.NewError(lib.CODE_FORBIDDEN, fmt.Sprintf("invalid entity relationship"))
	//}

	// update basic info
	updateBasicReq := model.RatingUpdateRequest{
		Name:          req.Name,
		Key:           req.Key,
		IsActive:      req.IsActive,
		RatingGroupId: &req.RatingGroupId,
		Image:         &req.Image,
	}

	updateAdjustReq := model.RatingAdjustRequest{
		MinValue: req.MinValue,
		MaxValue: req.MaxValue,
	}
	lib.Sync(updateBasicReq, rating)

	rating.UpdaterID = &currentUser
	if err := s.baseRepoRating.Update(tx, ctx, rating, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{rating.ID},
	}); err != nil {
		return nil, ginext.NewError(lib.CODE_BAD_REQUEST, err.Error())
	}

	// create transaction info + adjust
	latestAdjust := rating.RatingTransaction[0].RatingAdjust
	if (req.MinValue != nil && latestAdjust.MinValue != *req.MinValue) || (req.MaxValue != nil && latestAdjust.MaxValue != *req.MaxValue) {
		ratingTransaction := &model.RatingTransaction{
			BaseModel: model.BaseModel{
				CreatorID: &currentUser,
			},
			RatingID: rating.ID,
		}
		err = s.baseRepoRatingTransaction.Create(tx, ctx, ratingTransaction)
		if err != nil {
			return nil, err
		}
		lib.Sync(updateAdjustReq, latestAdjust)
		ratingAdjust := &model.RatingAdjust{
			BaseModel: model.BaseModel{
				CreatorID: &currentUser,
			},
			RatingTransactionID: ratingTransaction.ID,
			MinValue:            latestAdjust.MinValue,
			MaxValue:            latestAdjust.MaxValue,
		}
		err = s.baseRepoRatingAdjust.Create(tx, ctx, ratingAdjust)
		if err != nil {
			return nil, err
		}
	}

	rs, err := s.baseRepoRating.FindOne(tx, ctx, model.BaseRepoQueries{
		Preloads: map[string][]interface{}{
			"RatingTransaction": {
				func(db *gorm.DB) *gorm.DB {
					db = db.Preload("RatingAdjust").Order("created_at DESC")
					return db
				},
			},
			"RatingGroup": nil,
		},
		Condition: "id = ?",
		Args:      []interface{}{req.ID.String()},
	})
	if err != nil {
		return nil, err
	}

	return rs, nil
}

func (s *RatingService) UpdateRatingEntity(ctx context.Context, req *model.UpdateEntityRequest, currentUser uuid.UUID, entityTaxonomyID uuid.UUID) (*model.Rating, error) {
	var err error
	tx := s.baseRepoRating.RepoTxn().Begin()
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	rating, err := s.baseRepoRating.FindOne(nil, ctx, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{req.ID.String()},
	})
	if err != nil {
		return nil, ginext.NewError(lib.CODE_NOT_FOUND, err.Error())
	}

	//update entity relationship:
	// Remove tat ca cac entity relation hien tại cua object id
	err = s.relationshipService.RemoveEntityRelationshipByObject(tx, ctx, &model.EntityRelationshipByObjectRequest{
		ObjectId:    &rating.ID,
		ObjectType:  utils.ToPointer(utils.ObjectRating),
		CurrentUser: &currentUser,
	})
	if err != nil {
		return nil, err
	}

	// Update đè lên relationship hiện tại
	// get primary if null
	if req.EntityTermTaxonomyPrimary == nil {
		req.EntityTermTaxonomyPrimary = s.relationshipService.GetPrimaryEntityFromEntities(ctx, req.EntityTermTaxonomyIDs)
	}
	err = s.relationshipService.CreateEntityRelationshipByObject(tx, ctx, &model.EntityRelationshipByObjectRequest{
		ObjectId:                  &rating.ID,
		ObjectType:                utils.ToPointer(utils.ObjectRating),
		EntityTermTaxonomyIDs:     req.EntityTermTaxonomyIDs,
		EntityTermTaxonomyPrimary: req.EntityTermTaxonomyPrimary,
		CurrentUser:               &currentUser,
	})
	if err != nil {
		return nil, err
	}

	return rating, nil
}

func (s *RatingService) DeleteRating(ctx context.Context, req model.DeleteRequest, entityTaxonomyID uuid.UUID) error {
	//for _, rating := range req.ID {
	//	entityRelate, err := s.baseRepoEntityRelationship.
	//		FindOne(nil, ctx, model.BaseRepoQueries{
	//			Condition: "object_id = ? AND object_type = ? AND entity_term_taxonomy_id = ?",
	//			Args:      []interface{}{rating.String(), utils.ObjectRating, entityTaxonomyID.String()},
	//		})
	//	if err != nil {
	//		return ginext.NewError(lib.CODE_FORBIDDEN, fmt.Sprintf("invalid entity relationship"))
	//	}
	//	if entityRelate.EntityRelationshipType != model.RelationshipTypePrimary {
	//		return ginext.NewError(lib.CODE_FORBIDDEN, fmt.Sprintf("invalid entity relationship"))
	//	}
	//}
	return s.baseRepoRating.DeleteAction(nil, ctx, req)
}

func (s *RatingService) GetOneRating(ctx context.Context, id uuid.UUID) (*model.Rating, error) {
	return s.baseRepoRating.FindOne(nil, ctx, model.BaseRepoQueries{
		Preloads: map[string][]interface{}{
			"RatingTransaction": {
				func(db *gorm.DB) *gorm.DB {
					db = db.Preload("RatingAdjust").Order("created_at DESC")
					return db
				},
			},
		},
		Condition: "id = ?",
		Args:      []interface{}{id},
	})
}

func (s *RatingService) GetListRating(ctx context.Context, filter *model.Filter) (*model.FilterRatingResult, error) {
	req, ok := filter.Request.(model.RatingGetListRequest)
	if !ok {
		return nil, fmt.Errorf("invalid filter request")
	}

	queryParams := map[string]interface{}{}
	queryParams = utils.FilterILike(req.Name, queryParams, "name ILIKE ?")
	queryParams = utils.FilterIfNotNil(req.Key, queryParams, "key = ?")
	queryParams = utils.FilterIfNotNil(req.EntityTermTaxonomyIDs, queryParams, "entity_relationship.entity_term_taxonomy_id in (?)")
	queryParams = utils.FilterIfNotNil(req.RatingGroupIds, queryParams, "rating.rating_group_id in (?)")
	condition, args := utils.BuildQuery(queryParams, "AND")

	result, err := s.baseRepoRating.Filter(nil, ctx, filter, model.BaseRepoQueries{
		SortableFields: []string{"name", "created_at"},
		Joins: map[string][]interface{}{
			"inner join entity_relationship on entity_relationship.object_id = rating.id AND entity_relationship.object_type = ?": {utils.ObjectRating},
		},
		Preloads: map[string][]interface{}{
			"RatingGroup": nil,
			"RatingTransaction": {
				func(db *gorm.DB) *gorm.DB {
					db = db.Preload("RatingAdjust").Order("created_at DESC")
					return db
				},
			},
		},
		Condition: condition,
		Args:      args,
	})
	if err != nil {
		return nil, err
	}

	return &model.FilterRatingResult{
		Filter:  result.Filter,
		Records: result.Records,
	}, nil
}

func (s *RatingService) GetListRatingPublished(ctx context.Context, filter *model.Filter) (*model.FilterRatingPublishedResult, error) {
	req, ok := filter.Request.(model.RatingGetListRequest)
	if !ok {
		return nil, fmt.Errorf("invalid filter request")
	}

	queryParams := map[string]interface{}{}
	queryParams = utils.FilterILike(req.Name, queryParams, "name ILIKE ?")
	queryParams = utils.FilterIfNotNil(req.Key, queryParams, "key = ?")
	queryParams = utils.FilterIfNotNil(req.EntityTermTaxonomyIDs, queryParams, "entity_relationship.entity_term_taxonomy_id in (?)")
	queryParams = utils.FilterIfNotNil(req.RatingGroupIds, queryParams, "rating.rating_group_id in (?)")
	condition, args := utils.BuildQuery(queryParams, "AND")

	result, err := s.baseRepoRating.Filter(nil, ctx, filter, model.BaseRepoQueries{
		SortableFields: []string{"name", "created_at"},
		Joins: map[string][]interface{}{
			"inner join entity_relationship on entity_relationship.object_id = rating.id AND entity_relationship.object_type = ?": {utils.ObjectRating},
		},
		Preloads: map[string][]interface{}{
			"RatingGroup": nil,
			"RatingTransaction": {
				func(db *gorm.DB) *gorm.DB {
					db = db.Preload("RatingAdjust").Order("created_at DESC")
					return db
				},
			},
		},
		Condition: condition,
		Args:      args,
	})
	if err != nil {
		return nil, err
	}

	// TODO:
	// join published table?
	// tmp code; refactor this
	var rs []model.RatingPublishedResponse
	for _, record := range result.Records {
		published := s.publishingService.CurrentPublishingRatings(record.RatingTransaction)
		if published != nil {
			rs = append(rs, model.RatingPublishedResponse{
				BaseModel:           record.BaseModel,
				Name:                record.Name,
				Key:                 record.Key,
				IsActive:            record.IsActive,
				RatingGroupId:       record.RatingGroupId,
				Image:               record.Image,
				RatingGroup:         record.RatingGroup,
				RatingTransactionID: published.BaseModel.ID,
				MinValue:            published.RatingAdjust.MinValue,
				MaxValue:            published.RatingAdjust.MaxValue,
			})
		}
	}

	return &model.FilterRatingPublishedResult{
		Filter:  result.Filter,
		Records: rs,
	}, nil
}
