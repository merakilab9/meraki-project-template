package service

import (
	"context"
	"fmt"
	"meraki-project-template/pkg/model"
	"meraki-project-template/pkg/repo"
	"meraki-project-template/pkg/utils"

	"github.com/google/uuid"
)

type RatingDetailService struct {
	baseRepoEntityRelationship repo.RepositoryInterface[model.EntityRelationship]
	baseRepoRatingDetail       repo.RepositoryInterface[model.RatingDetail]
	baseRepoRatingTransaction  repo.RepositoryInterface[model.RatingTransaction]
	relationshipService        EntityRelationshipInterface
	ruleService                RuleDefaultInterface
}

func NewRatingDetailService(
	baseRepoEntityRelationship repo.RepositoryInterface[model.EntityRelationship],
	baseRepoRatingDetail repo.RepositoryInterface[model.RatingDetail],
	baseRepoRatingTransaction repo.RepositoryInterface[model.RatingTransaction],
	relationshipService EntityRelationshipInterface,
	ruleService RuleDefaultInterface) RatingDetailInterface {
	return &RatingDetailService{
		baseRepoEntityRelationship: baseRepoEntityRelationship,
		baseRepoRatingDetail:       baseRepoRatingDetail,
		baseRepoRatingTransaction:  baseRepoRatingTransaction,
		relationshipService:        relationshipService,
		ruleService:                ruleService}
}

type RatingDetailInterface interface {
	CreateRatingDetail(ctx context.Context, req *model.RatingDetailRequest, owner, entityTaxonomyID *uuid.UUID) (*model.RatingDetail, error)
	FilterRatingDetail(ctx context.Context, filter *model.Filter) (*model.FilterRatingDetailResult, error)
	GetOneRatingDetail(ctx context.Context, id uuid.UUID) (*model.RatingDetail, error)
	UpdateRatingDetail(ctx context.Context, ob *model.RatingDetail) error
	DeleteRatingDetail(ctx context.Context, req model.DeleteRequest) error
}

func (s *RatingDetailService) CreateRatingDetail(ctx context.Context, req *model.RatingDetailRequest, owner, entityTaxonomyID *uuid.UUID) (*model.RatingDetail, error) {

	ratingTransaction, err := s.baseRepoRatingTransaction.FindOne(nil, ctx, model.BaseRepoQueries{
		Preloads: map[string][]interface{}{
			"RatingAdjust": nil,
		},
		Condition: "id = ?",
		Args:      []interface{}{req.RatingTransactionID},
	})
	if err != nil {
		return nil, err
	}

	if ratingTransaction.RatingID != *req.RatingID {
		return nil, fmt.Errorf("invalid transaction_id and rating_id")
	}
	// valid rating min,max value
	if *req.CountingValue < ratingTransaction.RatingAdjust.MinValue {
		return nil, fmt.Errorf("invalid counting value rating: reach min rating")
	}
	if *req.CountingValue > ratingTransaction.RatingAdjust.MaxValue {
		return nil, fmt.Errorf("invalid counting value rating: reach max rating")
	}

	// use rule_default belongs to primary_entity_term of rating
	entityRelationship, err := s.baseRepoEntityRelationship.FindOne(nil, ctx, model.BaseRepoQueries{
		Condition: "object_type = ? AND object_id = ? AND entity_relationship_type = ?",
		Args:      []interface{}{utils.ObjectRating, ratingTransaction.RatingID.String(), model.RelationshipTypePrimary},
	})
	if err != nil {
		return nil, err
	}

	ratingDetail := &model.RatingDetail{
		BaseModel: model.BaseModel{
			CreatorID: owner,
		},
		ObjectId:            *req.ObjectId,
		ObjectType:          *req.ObjectType,
		CountingValue:       *req.CountingValue,
		RatingTransactionID: &ratingTransaction.ID,
	}

	if err := s.ruleService.ValidRule(ctx, model.ValidRuleRequest{
		EntityTermID: entityRelationship.EntityTermTaxonomyId,
		Type:         utils.ObjectRating,
		Data:         ratingDetail,
	}); err != nil {
		return nil, fmt.Errorf("rating detail failed to valid rule: %v", err)
	}

	err = s.baseRepoRatingDetail.Create(nil, ctx, ratingDetail)
	if err != nil {
		return nil, err
	}
	return ratingDetail, nil
}

func (s *RatingDetailService) UpdateRatingDetail(ctx context.Context, ob *model.RatingDetail) error {

	ratingTransaction, err := s.baseRepoRatingTransaction.FindOne(nil, ctx, model.BaseRepoQueries{
		Preloads: map[string][]interface{}{
			"RatingAdjust": nil,
		},
		Condition: "id = ?",
		Args:      []interface{}{ob.RatingTransactionID},
	})
	if err != nil {
		return err
	}

	entityRelationship, err := s.baseRepoEntityRelationship.FindOne(nil, ctx, model.BaseRepoQueries{
		Condition: "object_type = ? AND object_id = ? AND entity_relationship_type = ?",
		Args:      []interface{}{utils.ObjectRating, ratingTransaction.RatingID, model.RelationshipTypePrimary},
	})
	if err != nil {
		return err
	}

	if err := s.ruleService.ValidRule(ctx, model.ValidRuleRequest{
		EntityTermID: entityRelationship.EntityTermTaxonomyId,
		Type:         utils.ObjectRating,
		Data:         ob,
	}); err != nil {
		return fmt.Errorf("rating detail failed to valid rule: %v", err)
	}

	return s.baseRepoRatingDetail.Update(nil, ctx, ob, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{ob.ID},
	})
}

func (s *RatingDetailService) DeleteRatingDetail(ctx context.Context, req model.DeleteRequest) error {
	return s.baseRepoRatingDetail.DeleteAction(nil, ctx, req)
}

func (s *RatingDetailService) GetOneRatingDetail(ctx context.Context, id uuid.UUID) (*model.RatingDetail, error) {
	return s.baseRepoRatingDetail.FindOne(nil, ctx, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{id},
	})
}

func (s *RatingDetailService) FilterRatingDetail(ctx context.Context, filter *model.Filter) (*model.FilterRatingDetailResult, error) {
	req, ok := filter.Request.(model.RatingDetailListRequest)
	if !ok {
		return nil, fmt.Errorf("invalid filter request")
	}
	queryParams := map[string]interface{}{}

	queryParams = utils.FilterIfNotNil(req.ObjectId, queryParams, "object_id = ?")
	queryParams = utils.FilterIfNotNil(req.RatingTransactionID, queryParams, "rating_transaction_id = ?")
	queryParams = utils.FilterIfNotNil(req.ObjectType, queryParams, "object_type = ?")
	queryParams = utils.FilterIfNotNil(req.CountingValue, queryParams, "counting_value = ?")
	queryParams = utils.FilterIfNotNil(req.CreatorID, queryParams, "creator_id = ?")
	condition, args := utils.BuildQuery(queryParams, "AND")

	result, err := s.baseRepoRatingDetail.Filter(nil, ctx, filter, model.BaseRepoQueries{
		SortableFields: []string{"name", "created_at"},
		Condition:      condition,
		Args:           args,
	})
	if err != nil {
		return nil, err
	}

	return &model.FilterRatingDetailResult{
		Filter:  result.Filter,
		Records: result.Records,
	}, nil
}
