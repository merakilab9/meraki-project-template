package service

import (
	"context"
	"fmt"
	"meraki-project-template/pkg/model"
	"meraki-project-template/pkg/repo"
	"meraki-project-template/pkg/utils"

	"github.com/google/uuid"
)

type RatingGroupService struct {
	baseRepo repo.RepositoryInterface[model.RatingGroup]
}

func NewRatingGroupService(baseRepo repo.RepositoryInterface[model.RatingGroup]) RatingGroupInterface {
	return &RatingGroupService{baseRepo: baseRepo}
}

type RatingGroupInterface interface {
	CreateRatingGroup(ctx context.Context, ob *model.RatingGroup) error
	FilterRatingGroup(ctx context.Context, filter *model.Filter) (*model.FilterRatingGroupResult, error)
	GetOneRatingGroup(ctx context.Context, id uuid.UUID) (*model.RatingGroup, error)
	UpdateRatingGroup(ctx context.Context, ob *model.RatingGroup) error
	DeleteRatingGroup(ctx context.Context, req model.DeleteRequest) error
}

func (s *RatingGroupService) CreateRatingGroup(ctx context.Context, ob *model.RatingGroup) error {
	return s.baseRepo.Create(nil, ctx, ob)
}
func (s *RatingGroupService) UpdateRatingGroup(ctx context.Context, ob *model.RatingGroup) error {
	return s.baseRepo.Update(nil, ctx, ob, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{ob.ID},
	})
}

func (s *RatingGroupService) DeleteRatingGroup(ctx context.Context, req model.DeleteRequest) error {
	return s.baseRepo.DeleteAction(nil, ctx, req)
}

func (s *RatingGroupService) GetOneRatingGroup(ctx context.Context, id uuid.UUID) (*model.RatingGroup, error) {
	return s.baseRepo.FindOne(nil, ctx, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{id},
	})
}

func (s *RatingGroupService) FilterRatingGroup(ctx context.Context, filter *model.Filter) (*model.FilterRatingGroupResult, error) {
	req, ok := filter.Request.(model.RatingGroupListRequest)
	if !ok {
		return nil, fmt.Errorf("invalid filter request")
	}
	queryParams := map[string]interface{}{}
	queryParams = utils.FilterIfNotNil(req.Key, queryParams, "key = ?")
	queryParams = utils.FilterILike(req.Name, queryParams, "name ILIKE ?")
	queryParams = utils.FilterIfNotNil(req.IsActive, queryParams, "is_active = ?")
	condition, args := utils.BuildQuery(queryParams, "AND")

	result, err := s.baseRepo.Filter(nil, ctx, filter, model.BaseRepoQueries{
		SortableFields: []string{"name", "created_at"},
		Condition:      condition,
		Args:           args,
	})
	if err != nil {
		return nil, err
	}

	return &model.FilterRatingGroupResult{
		Filter:  result.Filter,
		Records: result.Records,
	}, nil
}
