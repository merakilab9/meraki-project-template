package service

import (
	"context"
	"fmt"
	"gitlab.com/merakilab9/common/ginext"
	"gitlab.com/merakilab9/common/lib"
	"gorm.io/gorm"
	"meraki-project-template/pkg/model"
	"meraki-project-template/pkg/repo"
	"meraki-project-template/pkg/utils"

	"github.com/google/uuid"
)

type CommentService struct {
	baseRepo                   repo.RepositoryInterface[model.Comment]
	baseRepoCommentTransaction repo.RepositoryInterface[model.CommentTransaction]
	baseRepoCommentAdjust      repo.RepositoryInterface[model.CommentAdjust]
	baseRepoCommentTaxonomy    repo.RepositoryInterface[model.CommentTaxonomy]
	baseRepoReasonDetail       repo.RepositoryInterface[model.ReasonDetail]
	baseRepoRuleDefault        repo.RepositoryInterface[model.RuleDefault]
	baseRepoUser               repo.RepositoryInterface[model.User]
	publishingService          PublishingInterface
	ruleService                RuleDefaultInterface
}

func NewCommentService(baseRepo repo.RepositoryInterface[model.Comment],
	baseRepoCommentTransaction repo.RepositoryInterface[model.CommentTransaction],
	baseRepoCommentAdjust repo.RepositoryInterface[model.CommentAdjust],
	baseRepoCommentTaxonomy repo.RepositoryInterface[model.CommentTaxonomy],
	baseRepoReasonDetail repo.RepositoryInterface[model.ReasonDetail],
	baseRepoRuleDefault repo.RepositoryInterface[model.RuleDefault],
	baseRepoUser repo.RepositoryInterface[model.User],
	publishingService PublishingInterface,
	ruleService RuleDefaultInterface) CommentInterface {
	return &CommentService{baseRepo: baseRepo,
		baseRepoCommentTransaction: baseRepoCommentTransaction,
		baseRepoCommentAdjust:      baseRepoCommentAdjust,
		baseRepoCommentTaxonomy:    baseRepoCommentTaxonomy,
		baseRepoReasonDetail:       baseRepoReasonDetail,
		baseRepoRuleDefault:        baseRepoRuleDefault,
		baseRepoUser:               baseRepoUser,
		publishingService:          publishingService,
		ruleService:                ruleService}
}

type CommentInterface interface {
	CreateComment(ctx context.Context, req *model.CommentRequest, owner, entityTaxonomyID uuid.UUID) (*model.Comment, error)
	UpdateComment(ctx context.Context, req *model.UpdateCommentRequest, currentUser, entityTaxonomyID uuid.UUID) (*model.Comment, error)
	UpdateCommentContent(ctx context.Context, req *model.UpdateCommentContentRequest, currentUser, entityTaxonomyID uuid.UUID) (*model.Comment, error)
	UpdateCommentStatus(ctx context.Context, req *model.UpdateTransactionCommentRequest, currentUser, entityTaxonomyID uuid.UUID) (*model.Comment, error)
	DeleteComment(ctx context.Context, req model.DeleteRequest, entityTaxonomyID uuid.UUID) error
	FilterComment(ctx context.Context, filter *model.Filter) (*model.FilterCommentResult, error)
	GetOneComment(ctx context.Context, id uuid.UUID) (*model.Comment, error)
	GetListCommentPublished(ctx context.Context, filter *model.Filter) (*model.FilterCommentPublishedResult, error)
}

func (s *CommentService) CreateComment(ctx context.Context, req *model.CommentRequest, owner, entityTaxonomyID uuid.UUID) (res *model.Comment, err error) {

	var (
		author                *model.User
		commentTaxonomyParent *model.CommentTaxonomy
	)

	tx := s.baseRepo.RepoTxn().Begin()
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	if req.ParentID != nil {
		commentTaxonomyParent, err = s.baseRepoCommentTaxonomy.FindOne(tx, ctx, model.BaseRepoQueries{
			Condition: "comment_id = ?",
			Args:      []interface{}{req.ParentID.String()},
		})
		if err != nil {
			return nil, fmt.Errorf("Invalid parent id: %v", err)
		}
		req.DisplayLevel = commentTaxonomyParent.Level + 1 // for valid comment display level
	}
	if req.AuthorID != nil && req.AuthorType != nil {
		if *req.AuthorType == utils.ObjectUser {
			author, err = s.baseRepoUser.FindOne(tx, ctx, model.BaseRepoQueries{
				Condition: "id = ?",
				Args:      []interface{}{req.AuthorID.String()},
			})
			if err != nil {
				return nil, err
			}
		}
	}

	//check rule service
	if err := s.ruleService.ValidRule(ctx, model.ValidRuleRequest{
		EntityTermID: entityTaxonomyID,
		Type:         utils.ObjectComment,
		Data:         req,
	}); err != nil {
		return nil, err
	}

	//create comment
	comment := &model.Comment{
		BaseModel: model.BaseModel{
			CreatorID: &owner,
		},
		ObjectId:   utils.UUID(req.ObjectId),
		ObjectType: utils.String(req.ObjectType),
		AuthorID:   utils.UUID(req.AuthorID),
		AuthorType: utils.String(req.AuthorType),
	}

	if err := s.baseRepo.Create(tx, ctx, comment); err != nil {
		return nil, err
	}

	//create comment taxonomy
	commentTaxonomy := &model.CommentTaxonomy{
		BaseModel: model.BaseModel{
			CreatorID: &owner,
		},
		CommentID: comment.ID,
		Level:     0,
	}
	// Has parent
	if commentTaxonomyParent != nil {
		commentTaxonomy.Parent = commentTaxonomyParent.CommentID
		commentTaxonomy.Level = commentTaxonomyParent.Level + 1
	}

	if err := s.baseRepoCommentTaxonomy.Create(tx, ctx, commentTaxonomy); err != nil {
		return nil, err
	}

	//create comment transaction
	commentTransaction := &model.CommentTransaction{
		BaseModel: model.BaseModel{
			CreatorID: &owner,
		},
		CommentID: comment.ID,
		Status:    model.CommentStatusWaitingApprove,
	}
	if err := s.baseRepoCommentTransaction.Create(tx, ctx, commentTransaction); err != nil {
		return nil, err
	}

	//create comment adjust
	if err := s.baseRepoCommentAdjust.Create(tx, ctx, &model.CommentAdjust{
		BaseModel: model.BaseModel{
			CreatorID: &owner,
		},
		CommentTransactionID: commentTransaction.ID,
		AuthorName:           author.Username,
		AuthorEmail:          req.AuthorEmail,
		AuthorPhone:          req.AuthorPhone,
		AuthorIP:             req.AuthorIP,
		Content:              req.Content,
	}); err != nil {
		return nil, err
	}

	rs, err := s.baseRepo.FindOne(tx, ctx, model.BaseRepoQueries{
		Preloads: map[string][]interface{}{
			"CommentTransaction": {
				func(db *gorm.DB) *gorm.DB {
					db = db.Preload("CommentAdjust").Order("created_at DESC")
					return db
				},
			},
		},
		Condition: "id = ?",
		Args:      []interface{}{comment.ID.String()},
	})
	if err != nil {
		return nil, err
	}

	return rs, nil
}
func (s *CommentService) UpdateCommentContent(ctx context.Context, req *model.UpdateCommentContentRequest, currentUser, entityTaxonomyID uuid.UUID) (*model.Comment, error) {
	var err error
	tx := s.baseRepo.RepoTxn().Begin()
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	comment, err := s.baseRepo.FindOne(nil, ctx, model.BaseRepoQueries{
		Preloads: map[string][]interface{}{
			"CommentTransaction": {
				func(db *gorm.DB) *gorm.DB {
					db = db.Preload("CommentAdjust").Where("id = ?", req.CommentTransactionID.String()).Order("created_at DESC")
					return db
				},
			},
		},
		Condition: "id = ?",
		Args:      []interface{}{req.ID.String()},
	})
	if err != nil {
		return nil, fmt.Errorf("invalid comment: %v", err)
	}

	// valid author/creator
	if *req.AuthorID != comment.AuthorID {
		return nil, ginext.NewError(lib.CODE_UNAUTHORIZED, "not author of comment")
	}

	//check rule service
	if err := s.ruleService.ValidRule(ctx, model.ValidRuleRequest{
		EntityTermID: entityTaxonomyID,
		Type:         utils.ObjectComment,
		Data: &model.CommentRequest{
			Content: req.Content,
		},
	}); err != nil {
		return nil, err
	}
	comment.UpdaterID = &currentUser
	latestAdjust := comment.CommentTransaction[0].CommentAdjust
	// overwrite current adjust
	if comment.CommentTransaction[0].Status == model.CommentStatusWaitingApprove {
		// update current adjust
		latestAdjust.Content = req.Content
		latestAdjust.UpdaterID = &currentUser
		if err := s.baseRepoCommentAdjust.Update(tx, ctx, latestAdjust, model.BaseRepoQueries{
			Condition: "id = ?",
			Args:      []interface{}{latestAdjust.ID.String()},
		}); err != nil {
			return nil, err
		}
	}
	// add new transaction
	if comment.CommentTransaction[0].Status != model.CommentStatusWaitingApprove {
		// create new transaction
		commentTransaction := &model.CommentTransaction{
			BaseModel: model.BaseModel{
				CreatorID: &currentUser,
			},
			CommentID: comment.ID,
		}
		if err := s.baseRepoCommentTransaction.Create(tx, ctx, commentTransaction); err != nil {
			return nil, err
		}
		// create new adjust
		if err := s.baseRepoCommentAdjust.Create(tx, ctx, &model.CommentAdjust{
			BaseModel: model.BaseModel{
				CreatorID: &currentUser,
			},
			CommentTransactionID: commentTransaction.ID,
			AuthorName:           latestAdjust.AuthorName,
			AuthorEmail:          latestAdjust.AuthorEmail,
			AuthorPhone:          latestAdjust.AuthorPhone,
			AuthorIP:             latestAdjust.AuthorIP,
			Content:              req.Content,
		}); err != nil {
			return nil, err
		}
	}

	rs, err := s.baseRepo.FindOne(tx, ctx, model.BaseRepoQueries{
		Preloads: map[string][]interface{}{
			"CommentTransaction": {
				func(db *gorm.DB) *gorm.DB {
					db = db.Preload("CommentAdjust").Order("created_at DESC")
					return db
				},
			},
		},
		Condition: "id = ?",
		Args:      []interface{}{comment.ID.String()},
	})
	if err != nil {
		return nil, err
	}

	return rs, nil
}
func (s *CommentService) UpdateCommentStatus(ctx context.Context, req *model.UpdateTransactionCommentRequest, currentUser, entityTaxonomyID uuid.UUID) (*model.Comment, error) {
	var err error
	tx := s.baseRepo.RepoTxn().Begin()
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	transaction, err := s.baseRepoCommentTransaction.FindOne(tx, ctx, model.BaseRepoQueries{
		Condition: "id = ? AND comment_id = ?",
		Args:      []interface{}{req.CommentTransactionID.String(), req.ID.String()},
	})
	if err != nil {
		return nil, ginext.NewError(lib.CODE_NOT_FOUND, "transaction error: "+err.Error())
	}

	transaction.UpdaterID = &currentUser
	transaction.Status = *req.Status
	if err := s.baseRepoCommentTransaction.Update(tx, ctx, transaction, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{transaction.ID.String()},
	}); err != nil {
		return nil, err
	}

	if req.ReasonIDs != nil && len(*req.ReasonIDs) != 0 {
		for _, v := range *req.ReasonIDs {
			reasonDetail := &model.ReasonDetail{
				BaseModel: model.BaseModel{
					CreatorID: &currentUser,
				},
				CommentTransactionID: transaction.ID,
				ReasonId:             uuid.MustParse(v),
			}
			if err := s.baseRepoReasonDetail.Create(tx, ctx, reasonDetail); err != nil {
				return nil, err
			}
		}
	}

	rs, err := s.baseRepo.FindOne(tx, ctx, model.BaseRepoQueries{
		Preloads: map[string][]interface{}{
			"CommentTransaction": {
				func(db *gorm.DB) *gorm.DB {
					db = db.Preload("CommentAdjust").Order("created_at DESC")
					return db
				},
			},
		},
		Condition: "id = ?",
		Args:      []interface{}{transaction.CommentID.String()},
	})
	if err != nil {
		return nil, err
	}

	return rs, nil
}
func (s *CommentService) UpdateComment(ctx context.Context, req *model.UpdateCommentRequest, currentUser, entityTaxonomyID uuid.UUID) (*model.Comment, error) {
	var err error
	tx := s.baseRepo.RepoTxn().Begin()
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	comment, err := s.baseRepo.FindOne(tx, ctx, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{req.ID.String()},
		Preloads: map[string][]interface{}{
			"CommentTransaction": {
				func(db *gorm.DB) *gorm.DB {
					db = db.Preload("CommentAdjust").Order("created_at DESC")
					return db
				},
			},
		},
	})
	if err != nil {
		return nil, ginext.NewError(lib.CODE_NOT_FOUND, "comment error: "+err.Error())
	}

	lib.Sync(*req, comment)
	comment.UpdaterID = &currentUser
	if err := s.baseRepo.Update(tx, ctx, comment, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{comment.ID.String()},
	}); err != nil {
		return nil, err
	}

	return comment, nil
}
func (s *CommentService) DeleteComment(ctx context.Context, req model.DeleteRequest, entityTaxonomyID uuid.UUID) error {
	// TODO: check primary entity
	return s.baseRepo.DeleteAction(nil, ctx, req)
}

func (s *CommentService) GetOneComment(ctx context.Context, id uuid.UUID) (*model.Comment, error) {
	return s.baseRepo.FindOne(nil, ctx, model.BaseRepoQueries{
		Preloads: map[string][]interface{}{
			"CommentTransaction": {
				func(db *gorm.DB) *gorm.DB {
					db = db.Preload("CommentAdjust").Order("created_at DESC")
					return db
				},
			},
		},
		Condition: "id = ?",
		Args:      []interface{}{id},
	})
}

func (s *CommentService) FilterComment(ctx context.Context, filter *model.Filter) (*model.FilterCommentResult, error) {
	req, ok := filter.Request.(model.CommentListRequest)
	if !ok {
		return nil, fmt.Errorf("invalid filter request")
	}
	queryParams := map[string]interface{}{}
	queryParams = utils.FilterIfNotNil(req.IsSpam, queryParams, "is_spam = ?")
	queryParams = utils.FilterIfNotNil(req.IsHidden, queryParams, "is_hidden = ?")
	queryParams = utils.FilterIfNotNil(req.AuthorID, queryParams, "author_id = ?")
	queryParams = utils.FilterIfNotNil(req.AuthorType, queryParams, "author_type = ?")
	queryParams = utils.FilterIfNotNil(req.ObjectType, queryParams, "object_type = ?")
	queryParams = utils.FilterIfNotNil(req.ObjectId, queryParams, "object_id = ?")
	condition, args := utils.BuildQuery(queryParams, "AND")

	result, err := s.baseRepo.Filter(nil, ctx, filter, model.BaseRepoQueries{
		SortableFields: []string{"name", "created_at"},
		Preloads: map[string][]interface{}{
			"CommentTransaction": {
				func(db *gorm.DB) *gorm.DB {
					db = db.Preload("CommentAdjust").Order("created_at DESC")
					return db
				},
			},
		},
		Condition: condition,
		Args:      args,
	})

	if err != nil {
		return nil, err
	}
	return &model.FilterCommentResult{
		Filter:  result.Filter,
		Records: result.Records,
	}, nil
}

func (s *CommentService) GetListCommentPublished(ctx context.Context, filter *model.Filter) (*model.FilterCommentPublishedResult, error) {
	req, ok := filter.Request.(model.CommentListRequest)
	if !ok {
		return nil, fmt.Errorf("invalid filter request")
	}

	queryParams := map[string]interface{}{}
	queryParams = utils.FilterIfNotNil(req.ObjectId, queryParams, "object_id = ?")
	queryParams = utils.FilterIfNotNil(req.ObjectType, queryParams, "object_type = ?")
	queryParams = utils.FilterIfNotNil(utils.ToPointer(false), queryParams, "is_hidden = ?")
	queryParams = utils.FilterIfNotNil(utils.ToPointer(false), queryParams, "is_spam = ?")
	condition, args := utils.BuildQuery(queryParams, "AND")

	result, err := s.baseRepo.Filter(nil, ctx, filter, model.BaseRepoQueries{
		SortableFields: []string{"name", "created_at"},
		Preloads: map[string][]interface{}{
			"CommentTransaction": {
				func(db *gorm.DB) *gorm.DB {
					db = db.Preload("CommentAdjust").Where("status = ?", model.CommentStatusApproved).Order("created_at DESC")
					return db
				},
			},
		},
		Condition: condition,
		Args:      args,
	})
	if err != nil {
		return nil, err
	}

	// TODO:
	// join published table?
	// tmp code; refactor this
	var rs []model.CommentPublishedResponse
	for _, record := range result.Records {
		published := s.publishingService.CurrentPublishingComments(record.CommentTransaction)
		if published != nil {
			rs = append(rs, model.CommentPublishedResponse{
				BaseModel:            record.BaseModel,
				AuthorID:             record.AuthorID,
				AuthorType:           record.AuthorType,
				IsHidden:             record.IsHidden,
				IsSpam:               record.IsSpam,
				CommentTransactionID: &published.ID,
				AuthorName:           published.CommentAdjust.AuthorName,
				AuthorEmail:          published.CommentAdjust.AuthorEmail,
				AuthorPhone:          published.CommentAdjust.AuthorPhone,
				AuthorIP:             published.CommentAdjust.AuthorIP,
				Content:              published.CommentAdjust.Content,
				Status:               published.Status,
			})
		}
	}

	// todo: comment tree

	return &model.FilterCommentPublishedResult{
		Filter:  result.Filter,
		Records: rs,
	}, nil
}
