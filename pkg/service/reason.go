package service

import (
	"context"
	"fmt"
	"gitlab.com/merakilab9/common/ginext"
	"gitlab.com/merakilab9/common/lib"
	"meraki-project-template/pkg/model"
	"meraki-project-template/pkg/repo"
	"meraki-project-template/pkg/utils"

	"github.com/google/uuid"
)

type ReasonService struct {
	baseRepo            repo.RepositoryInterface[model.Reason]
	relationshipService EntityRelationshipInterface
}

func NewReasonService(relationshipService EntityRelationshipInterface, baseRepo repo.RepositoryInterface[model.Reason]) ReasonInterface {
	return &ReasonService{relationshipService: relationshipService, baseRepo: baseRepo}
}

type ReasonInterface interface {
	CreateReason(ctx context.Context, req *model.ReasonRequest, owner uuid.UUID) (*model.Reason, error)
	FilterReason(ctx context.Context, filter *model.Filter) (*model.FilterReasonResult, error)
	GetOneReason(ctx context.Context, id uuid.UUID) (*model.Reason, error)
	UpdateReason(ctx context.Context, ob *model.Reason) error
	UpdateReasonEntity(ctx context.Context, req *model.UpdateEntityRequest, currentUser uuid.UUID, entityTaxonomyID uuid.UUID) (*model.Reason, error)
	DeleteReason(ctx context.Context, req model.DeleteRequest) error
}

func (s *ReasonService) CreateReason(ctx context.Context, req *model.ReasonRequest, owner uuid.UUID) (*model.Reason, error) {
	var err error
	tx := s.baseRepo.RepoTxn().Begin()
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	reason := &model.Reason{
		BaseModel: model.BaseModel{
			CreatorID: &owner,
		},
	}
	lib.Sync(*req, reason)

	err = s.baseRepo.Create(tx, ctx, reason)
	if err != nil {
		return nil, err
	}

	if req.EntityTermTaxonomyPrimary == nil {
		req.EntityTermTaxonomyPrimary = s.relationshipService.GetPrimaryEntityFromEntities(ctx, req.EntityTermTaxonomyIDs)
	}
	err = s.relationshipService.CreateEntityRelationshipByObject(tx, ctx, &model.EntityRelationshipByObjectRequest{
		ObjectId:                  &reason.ID,
		ObjectType:                utils.ToPointer(utils.ObjectReason),
		EntityTermTaxonomyIDs:     req.EntityTermTaxonomyIDs,
		EntityTermTaxonomyPrimary: req.EntityTermTaxonomyPrimary,
		CurrentUser:               &owner,
	})
	if err != nil {
		return nil, err
	}
	return reason, nil
}

func (s *ReasonService) UpdateReason(ctx context.Context, ob *model.Reason) error {
	return s.baseRepo.Update(nil, ctx, ob, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{ob.ID},
	})
}

func (s *ReasonService) UpdateReasonEntity(ctx context.Context, req *model.UpdateEntityRequest, currentUser uuid.UUID, entityTaxonomyID uuid.UUID) (*model.Reason, error) {
	var err error
	tx := s.baseRepo.RepoTxn().Begin()
	defer func() {
		if err != nil {
			tx.Rollback()
		} else {
			tx.Commit()
		}
	}()

	rule, err := s.baseRepo.FindOne(nil, ctx, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{req.ID.String()},
	})
	if err != nil {
		return nil, ginext.NewError(lib.CODE_NOT_FOUND, err.Error())
	}

	//update entity relationship:
	// Remove tat ca cac entity relation hien tại cua object id
	err = s.relationshipService.RemoveEntityRelationshipByObject(tx, ctx, &model.EntityRelationshipByObjectRequest{
		ObjectId:    &rule.ID,
		ObjectType:  utils.ToPointer(utils.ObjectReason),
		CurrentUser: &currentUser,
	})
	if err != nil {
		return nil, err
	}

	// Update đè lên relationship hiện tại
	// get primary if null
	if req.EntityTermTaxonomyPrimary == nil {
		req.EntityTermTaxonomyPrimary = s.relationshipService.GetPrimaryEntityFromEntities(ctx, req.EntityTermTaxonomyIDs)
	}
	err = s.relationshipService.CreateEntityRelationshipByObject(tx, ctx, &model.EntityRelationshipByObjectRequest{
		ObjectId:                  &rule.ID,
		ObjectType:                utils.ToPointer(utils.ObjectReason),
		EntityTermTaxonomyIDs:     req.EntityTermTaxonomyIDs,
		EntityTermTaxonomyPrimary: req.EntityTermTaxonomyPrimary,
		CurrentUser:               &currentUser,
	})
	if err != nil {
		return nil, err
	}

	return rule, nil
}

func (s *ReasonService) DeleteReason(ctx context.Context, req model.DeleteRequest) error {
	return s.baseRepo.DeleteAction(nil, ctx, req)
}

func (s *ReasonService) GetOneReason(ctx context.Context, id uuid.UUID) (*model.Reason, error) {
	return s.baseRepo.FindOne(nil, ctx, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{id},
	})
}

func (s *ReasonService) FilterReason(ctx context.Context, filter *model.Filter) (*model.FilterReasonResult, error) {
	req, ok := filter.Request.(model.ReasonListRequest)
	if !ok {
		return nil, fmt.Errorf("invalid filter request")
	}
	queryParams := map[string]interface{}{}
	queryParams = utils.FilterILike(req.Name, queryParams, "name ILIKE ?")
	queryParams = utils.FilterIfNotNil(req.Key, queryParams, "key = ?")
	queryParams = utils.FilterIfNotNil(req.IsActive, queryParams, "is_active = ?")
	queryParams = utils.FilterIfNotNil(req.EntityTermTaxonomyIDs, queryParams, "entity_relationship.entity_term_taxonomy_id in (?)")
	condition, args := utils.BuildQuery(queryParams, "AND")
	result, err := s.baseRepo.Filter(nil, ctx, filter, model.BaseRepoQueries{
		SortableFields: []string{"name", "created_at"},
		Joins: map[string][]interface{}{
			"inner join entity_relationship on entity_relationship.object_id = reason.id AND entity_relationship.object_type = ?": {utils.ObjectReason},
		},
		Condition: condition,
		Args:      args,
	})
	if err != nil {
		return nil, err
	}

	return &model.FilterReasonResult{
		Filter:  result.Filter,
		Records: result.Records,
	}, nil
}
