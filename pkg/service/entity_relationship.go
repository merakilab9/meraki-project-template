package service

import (
	"context"
	"fmt"
	"gitlab.com/merakilab9/common/lib"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
	"meraki-project-template/pkg/model"
	"meraki-project-template/pkg/repo"
	"meraki-project-template/pkg/utils"

	"github.com/google/uuid"
)

type EntityRelationshipService struct {
	baseRepo repo.RepositoryInterface[model.EntityRelationship]
}

func NewEntityRelationshipService(baseRepo repo.RepositoryInterface[model.EntityRelationship]) EntityRelationshipInterface {
	return &EntityRelationshipService{baseRepo: baseRepo}
}

type EntityRelationshipInterface interface {
	CreateEntityRelationship(ctx context.Context, ob *model.EntityRelationship) error
	FilterEntityRelationship(ctx context.Context, filter *model.Filter) (*model.FilterEntityRelationshipResult, error)
	GetOneEntityRelationship(ctx context.Context, id uuid.UUID) (*model.EntityRelationship, error)
	UpdateEntityRelationship(ctx context.Context, ob *model.EntityRelationship) error
	DeleteEntityRelationship(ctx context.Context, req model.DeleteRequest) error
	CreateEntityRelationshipByObject(tx *gorm.DB, ctx context.Context, req *model.EntityRelationshipByObjectRequest) error
	RemoveEntityRelationshipByObject(tx *gorm.DB, ctx context.Context, req *model.EntityRelationshipByObjectRequest) error
	CheckEntityHasPrimary(ctx context.Context, entityTermTaxonomyId uuid.UUID) bool
	GetPrimaryEntityFromEntities(ctx context.Context, entityIds []uuid.UUID) *uuid.UUID
	GetListReference(ctx context.Context, entity_ids uuid.UUID) []uuid.UUID
}

func (s *EntityRelationshipService) CreateEntityRelationship(ctx context.Context, ob *model.EntityRelationship) error {
	return s.baseRepo.Create(nil, ctx, ob)
}
func (s *EntityRelationshipService) UpdateEntityRelationship(ctx context.Context, ob *model.EntityRelationship) error {
	return s.baseRepo.Update(nil, ctx, ob, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{ob.ID},
	})
}

func (s *EntityRelationshipService) DeleteEntityRelationship(ctx context.Context, req model.DeleteRequest) error {
	return s.baseRepo.DeleteAction(nil, ctx, req)
}

func (s *EntityRelationshipService) GetOneEntityRelationship(ctx context.Context, id uuid.UUID) (*model.EntityRelationship, error) {
	return s.baseRepo.FindOne(nil, ctx, model.BaseRepoQueries{
		Condition: "id = ?",
		Args:      []interface{}{id},
	})
}

func (s *EntityRelationshipService) FilterEntityRelationship(ctx context.Context, filter *model.Filter) (*model.FilterEntityRelationshipResult, error) {
	req, ok := filter.Request.(model.RuleListRequest)
	if !ok {
		return nil, fmt.Errorf("invalid filter request")
	}
	queryParams := map[string]interface{}{}
	queryParams = utils.FilterIfNotNil(req.Name, queryParams, "name ILIKE %?%")
	queryParams = utils.FilterIfNotNil(req.Type, queryParams, "type = ?")
	condition, args := utils.BuildQuery(queryParams, "AND")

	result, err := s.baseRepo.Filter(nil, ctx, filter, model.BaseRepoQueries{
		SortableFields: []string{"name", "created_at"},
		Condition:      condition,
		Args:           args,
	})
	if err != nil {
		return nil, err
	}

	return &model.FilterEntityRelationshipResult{
		Filter:  result.Filter,
		Records: result.Records,
	}, nil
}

func (s *EntityRelationshipService) CreateEntityRelationshipByObject(tx *gorm.DB, ctx context.Context, req *model.EntityRelationshipByObjectRequest) error {

	if err := lib.CheckRequireValid(req); err != nil {
		return err
	}

	if err := s.baseRepo.Create(tx, ctx, &model.EntityRelationship{
		BaseModel: model.BaseModel{
			CreatorID: req.CurrentUser,
		},
		ObjectId:               *req.ObjectId,
		ObjectType:             *req.ObjectType,
		EntityTermTaxonomyId:   *req.EntityTermTaxonomyPrimary,
		EntityRelationshipType: model.RelationshipTypePrimary,
	}); err != nil {
		return err
	}

	var refEntity []*model.EntityRelationship
	for _, entity := range req.EntityTermTaxonomyIDs {
		refEntity = append(refEntity, &model.EntityRelationship{
			BaseModel: model.BaseModel{
				CreatorID: req.CurrentUser,
			},
			ObjectId:               *req.ObjectId,
			ObjectType:             *req.ObjectType,
			EntityTermTaxonomyId:   entity,
			EntityRelationshipType: model.RelationshipTypeReference,
		})
	}
	if _, err := s.baseRepo.Creates(tx, ctx, refEntity, model.BaseRepoQueries{
		Clauses: []clause.Expression{clause.OnConflict{DoNothing: true}},
	}); err != nil {
		return err
	}

	return nil
}

func (s *EntityRelationshipService) RemoveEntityRelationshipByObject(tx *gorm.DB, ctx context.Context, req *model.EntityRelationshipByObjectRequest) error {
	relationships, err := s.baseRepo.Find(tx,
		ctx, model.BaseRepoQueries{
			Condition: "object_id = ? AND object_type = ? ",
			Args:      []interface{}{req.ObjectId.String(), req.ObjectType},
		})
	if err != nil {
		return err
	}

	var listRelationsID []uuid.UUID
	for _, relation := range relationships {
		listRelationsID = append(listRelationsID, relation.ID)
	}
	if err := s.baseRepo.DeleteUnscope(tx, ctx, listRelationsID); err != nil {
		return err
	}

	return nil
}

func (s *EntityRelationshipService) CheckEntityHasPrimary(ctx context.Context, entityTermTaxonomyId uuid.UUID) bool {
	return true
}

func (s *EntityRelationshipService) GetPrimaryEntityFromEntities(ctx context.Context, entityIds []uuid.UUID) *uuid.UUID {
	// TODO: get highest entity from list
	return &entityIds[0]
}

func (s *EntityRelationshipService) GetListReference(ctx context.Context, entity_id uuid.UUID) []uuid.UUID {
	return []uuid.UUID{entity_id}
}
