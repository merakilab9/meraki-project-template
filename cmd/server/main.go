package main

import (
	"context"
	"meraki-project-template/conf"
	"meraki-project-template/pkg/route"
	"os"

	"gitlab.com/merakilab9/common/logger"
)

func main() {
	logger.Init("meraki-project-template")
	conf.LoadConfig()

	app := route.NewService()
	ctx := context.Background()
	err := app.Start(ctx)
	if err != nil {
		logger.Tag("main").Error(err)
	}
	os.Clearenv()
}
